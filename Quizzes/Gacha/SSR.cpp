#include "SSR.h"

SSR::SSR(string name, int rollChance, int pullCost, int rarityPoint) : RareItem(name, rollChance, pullCost, rarityPoint)
{
}

SSR::~SSR()
{
}

void SSR::executeEffect(Player *player)
{
	cout << "You received " << mRarityPoint << " Rarity Points!" << endl;
	player->addRarityPoint(mRarityPoint);
}

Item* SSR::clone()
{
	return new SSR(*this);
}

void SSR::incrementCounter(Player* player)
{
	player->incrementSSRCounter();
}
