#pragma once
#include "Item.h"

class HealthPotion :public Item
{
public:
	HealthPotion(string name, int rollChance, int pullCost, int healValue);
	~HealthPotion();

	void executeEffect(Player* player);
	Item* clone();

	// Accessor function
	void incrementCounter(Player* player);

private: 
	int mHealValue;
};

