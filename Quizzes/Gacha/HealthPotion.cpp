#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int rollChance, int pullCost, int healValue) : Item(name, rollChance, pullCost)
{
	mHealValue = healValue;
}

HealthPotion::~HealthPotion()
{
}

void HealthPotion::executeEffect(Player* player)
{
	// Heal the player
	cout << "You have been healed for " << mHealValue << "!" << endl;
	player->heal(mHealValue);
}

Item* HealthPotion::clone()
{
	return new HealthPotion(*this);
}

void HealthPotion::incrementCounter(Player* player)
{
	player->incrementHealthPotionCounter();
}

