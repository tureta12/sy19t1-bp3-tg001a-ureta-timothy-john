#include "Item.h"
#include "Player.h"

Item::Item(string name, int rollChance, int pullCost)
{
	mName = name;
	mRollChance = rollChance;
	mPullCost = pullCost;
}

Item::~Item()
{
}

void Item::executeEffect(Player* player)
{
}

Item* Item::clone()
{
	return new Item(*this);
}

void Item::incrementCounter(Player* player)
{
}

int Item::getRollChance()
{
	return mRollChance;
}

int Item::getPullCost()
{
	return mPullCost;
}

string Item::getName()
{
	return mName;
}


