//#include "Bomb.h"
//#include "Crystal.h"
#include "Player.h"
#include "Item.h"
//#include "R.h"
//#include "RareItem.h"
//#include "SR.h"
//#include "SSR.h"

Player::Player(string name)
{
	mName = name;
	mHP = 100; // Player starts at 100 HP
	mCrystals = 100; // Player starts at 100 Crystals
	mPulls = 0;
	mRarityPoints = 0;

	mHealthPotionCounter = 0;
	mBombCounter = 0;
	mCrystalCounter = 0;
	mRCounter = 0;
	mSRCounter = 0;
	mSSRCounter = 0;
}

Player::~Player()
{
	cout << "Player object deleted, memory freed!" << endl;

	// Delete the vector of pulled items
	for (int i = 0; i < mPulledItems.size(); i++)
	{
		delete mPulledItems[i];
	}

	mPulledItems.clear();
}


void Player::Pull(GachaPool* pool)
{
	// Get roll 
	int roll = rand() % 100 + 1;

	// Move through the pullPool vector until the roll is less than or equal to the item rollChance
	// pullPool has been manually sorted in order of rarity from lowest chance to highest. 
	int rolledItemIndex = 0;
	for (int i = 0; i < pool->getPullPool().size(); i++)
	{
		int itemRollChance = pool->getPullPool()[i]->getRollChance();
		if (roll <= itemRollChance)
		{
			// An item has been successfully rolled 
			rolledItemIndex = i;
			// cout << "This is a test. The Rolled item index is:" << rolledItemIndex << endl;
			break; 
		}
	}

	Item* objectCopied = pool->getPullPool()[rolledItemIndex]->clone();
	cout << "You pulled a " << objectCopied->getName() << "!" << endl;
	objectCopied->executeEffect(this); //Taking the player as reference

	// minus crystals
	reduceCrystals(objectCopied->getPullCost());

	// Add to player's collection of pulled items and increment pull counter
	mPulledItems.push_back(objectCopied);
	mPulls++;

	//Updates specific item counters
	objectCopied->incrementCounter(this);
}

void Player::takeDamage(int value)
{
	mHP -= value;
}

void Player::heal(int value)
{
	mHP += value;
}

void Player::reduceCrystals(int value)
{
	mCrystals -= value;
}

void Player::addCrystals(int value)
{
	mCrystals += value;
}

void Player::addRarityPoint(int value)
{
	mRarityPoints += value;
}

void Player::incrementHealthPotionCounter()
{
	mHealthPotionCounter++;
}

void Player::incrementBombCounter()
{
	mBombCounter++;
}

void Player::incrementCrystalCounter()
{
	mCrystalCounter++;
}

void Player::incrementRCounter()
{
	mRCounter++;
}

void Player::incrementSRCounter()
{
	mSRCounter++;
}

void Player::incrementSSRCounter()
{
	mSSRCounter++;
}

string Player::getName()
{
	return string();
}

int Player::getRarityPoints()
{
	return mRarityPoints;
}

vector<Item*> Player::getPlayerPulledItems()
{
	return mPulledItems;
}

void Player::printStats()
{
	cout << "HP: " << mHP << endl;
	cout << "Cystals: " << mCrystals << endl;
	cout << "Rarity Points: " << mRarityPoints << endl;
	cout << "Pulls: " << mPulls << endl;
}

void Player::printPulledItems()
{
	cout << "Printing out all collected items..." << endl;
	for (int i = 0; i < mPulledItems.size(); i++)
	{
		cout << mPulledItems[i]->getName() << endl;
	}
	system("pause");
}

void Player::printCollatedItems()
{
	cout << "You pulled " << mPulledItems.size() << " items!\n" << endl;
	cout << "SSR " << "x" << mSSRCounter << endl;
	cout << "SR " << " x" << mSRCounter << endl;
	cout << "R " << " x" << mRCounter << endl;
	cout << "Crystal" << " x" << mCrystalCounter << endl;
	cout << "Health Potion" << " x" << mHealthPotionCounter << endl;
	cout << "Bomb" << " x" << mBombCounter << endl;
}

bool Player::canPull()
{
	if (mCrystals <= 0)
		return false;
	else
		return true;
}

bool Player::isAlive()
{
	if (mHP <= 0)
		return false;
	else
		return true;
}
