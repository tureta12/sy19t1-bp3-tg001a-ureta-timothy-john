#include "R.h"

R::R(string name, int rollChance, int pullCost, int rarityPoint) : RareItem(name, rollChance, pullCost, rarityPoint)
{
}

R::~R()
{
}

void R::executeEffect(Player *player)
{
	cout << "You received " << mRarityPoint << " Rarity Points!" << endl;
	player->addRarityPoint(mRarityPoint);
}

Item* R::clone()
{
	return new R(*this);
}

void R::incrementCounter(Player* player)
{
	player->incrementRCounter();
}
