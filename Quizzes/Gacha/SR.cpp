#include "SR.h"

SR::SR(string name, int rollChance, int pullCost, int rarityPoint) : RareItem(name, rollChance, pullCost, rarityPoint)
{
}

SR::~SR()
{
}

void SR::executeEffect(Player *player)
{
	cout << "You received " << mRarityPoint << " Rarity Points!" << endl;
	player->addRarityPoint(mRarityPoint);
}

Item* SR::clone()
{
	return new SR(*this);
}

void SR::incrementCounter(Player* player)
{
	player->incrementSRCounter();
}
