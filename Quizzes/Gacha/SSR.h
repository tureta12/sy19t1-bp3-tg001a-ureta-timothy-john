#pragma once
#include "RareItem.h"

class SSR : public RareItem
{
public:
	SSR(string name, int rollChance, int pullCost, int rarityPoint);
	~SSR();

	void executeEffect(Player *player);
	Item* clone();

	// Accessor function
	void incrementCounter(Player* player);
};

