#include "RareItem.h"

RareItem::RareItem(string name, int rollChance, int pullCost, int rarityPoint) : Item(name, rollChance, pullCost)
{
	mRarityPoint = rarityPoint;
}

RareItem::~RareItem()
{
}
