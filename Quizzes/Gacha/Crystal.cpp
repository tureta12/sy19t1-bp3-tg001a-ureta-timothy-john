#include "Crystal.h"

Crystal::Crystal(string name, int rollChance, int pullCost, int valueCrystal) : Item(name, rollChance, pullCost)
{
	mValueCrystal = valueCrystal;
}

Crystal::~Crystal()
{
}

void Crystal::executeEffect(Player* player)
{
	// Give the player more crystals
	cout << "You've earned " << mValueCrystal << " crystals!" << endl;
	player->addCrystals(mValueCrystal);
}

Item* Crystal::clone()
{
	return new Crystal(*this);
}

void Crystal::incrementCounter(Player* player)
{
	player->incrementCrystalCounter();
}
