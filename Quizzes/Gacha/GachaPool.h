#pragma once
#include <vector>

class Item; //forward declaration

using namespace std;

class GachaPool
{
public:
	GachaPool();
	~GachaPool();

	// Getter function
	vector<Item*> getPullPool();

private:
	vector<Item*> pullPool;
};

