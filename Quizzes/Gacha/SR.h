#pragma once
#include "RareItem.h"

class SR : public RareItem
{
public:
	SR(string name, int rollChance, int pullCost, int rarityPoint);
	~SR();

	void executeEffect(Player *player);
	Item* clone();

	// Accessor function
	void incrementCounter(Player* player);
};

