#pragma once
#include "Player.h"
#include <iostream>

using namespace std;

class Item
{
public:
	Item(string name, int rollChance, int pullCost);
	~Item();
	
	virtual void executeEffect(Player *player);
	virtual Item* clone();

	// Accessor function
	virtual void incrementCounter(Player* player);

	// Getter function
	int getRollChance();
	int getPullCost();
	string getName();

protected:
	string mName;
	int mRollChance;
	int mPullCost;
};

