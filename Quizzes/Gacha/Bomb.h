#pragma once
#include "Item.h"

class Bomb : public Item
{
public:
	Bomb(string name, int rollChance, int pullCost, int damage);
	~Bomb();

	void executeEffect(Player* player); 
	Item* clone();

	// Accessor function
	void incrementCounter(Player* player);

private:
	int mDamage;
};

