#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"

using namespace std;

int main()
{
	// Declarations and Initializations
	srand(time(NULL));
	string userName;
	int rarityPointGoal = 100; // Player needs 100 rarity points to win. Adjust for difficulty 
	GachaPool* thisPool = new GachaPool();

	// Ask user for name and create a Player object with that name 
	cout << "What's your name? ";
	cin >> userName;
	cout << "Goodluck " << userName << "!" << endl;
	system("pause");
	system("cls");
	Player* player = new Player(userName);

	// Main game loop
	while ( (player->isAlive() && player->canPull()) && (player->getRarityPoints() < rarityPointGoal) )
	{
		// Print stats
		player->printStats();
		cout << "\n";

		// Pull
		player->Pull(thisPool);
		system("pause");
		system("cls");
	}

	// Win-lose conditions
	if ( player->isAlive() )
	{
		if ( player->getRarityPoints() >= rarityPointGoal )
		{
			cout << "You've gathered enough rarity points!" << endl;
		}
		else
		{
			cout << "You ran out of crystals!" << endl;
		}
	}
	else if ( player->canPull() )
	{
		cout << "You ran out of HP!" << endl;
	}
	
	// Game Summary 
	cout << "===============================" << endl;
	player->printStats();
	cout << "===============================\n" << endl;
	cout << "Items Pulled:" << endl;
	cout << "-------------------------------" << endl;
	player->printPulledItems();
	cout << endl;
	cout << "-------------------------------" << endl;
	cout << "Collated items:" << endl;
	player->printCollatedItems();
	cout << "-------------------------------" << endl;
	system("pause");
	cout << endl;

	// Delete stuff?
	delete thisPool;
	delete player;

	return 0;
}