#include "Bomb.h"
#include "Crystal.h"
#include "GachaPool.h"
#include "HealthPotion.h"
#include "Item.h"
#include "Player.h"
#include "R.h"
#include "RareItem.h"
#include "SR.h"
#include "SSR.h"

GachaPool::GachaPool()
{
	// Table of items from the pool and their chances
	// Item			   %        Corresponding "roll"    #toPutForTheirChance
	// SSR			 - 1%		1								1
	// SR		     - 9%		2-10							10
	// Crystals      - 15%		11-25							25
	// Health Potion - 15%		26-40							40
	// Bomb          - 20%		41-60							60
	// R			 - 40%		61-100							100

	// Initialize items to be placed in the pool
	pullPool.push_back(new SSR("SSR", 1, 5, 50)); // Name, rollChance, pullCost, rarityPoint
	pullPool.push_back(new SR("SR", 10, 5, 10)); // Name, rollChance, pullCost, rarityPoint
	pullPool.push_back(new HealthPotion("Health Potion", 25, 5, 30)); // Name, rollChance, pullCost healingValue
	pullPool.push_back(new Crystal("Crystal", 40, 5, 15)); // Name, rollChance, pullCost, crystalsAdded
	pullPool.push_back(new Bomb("Bomb", 60, 5, 25)); // Name, rollChance, pullCost damageValue
	pullPool.push_back(new R("R", 100, 5, 1)); // Name, rollChance, pullCost, rarityPoint
}

GachaPool::~GachaPool()
{
	// Iterate through the vector, deleting the instances made and then clearing the pointers. 
	for (int i = 0; i < pullPool.size(); i++)
	{
		delete pullPool[i];
	}

	pullPool.clear();
	cout << "GachaPool object deleted, memory cleared!" << endl;
}

vector<Item*> GachaPool::getPullPool()
{
	return pullPool;
}
