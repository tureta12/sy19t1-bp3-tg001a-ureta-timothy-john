#pragma once
#include "RareItem.h"

class R : public RareItem
{
public:
	R(string name, int rollChance, int pullCost, int rarityPoint);
	~R();

	void executeEffect(Player *player);
	Item* clone();

	// Accessor function
	void incrementCounter(Player* player);
};

