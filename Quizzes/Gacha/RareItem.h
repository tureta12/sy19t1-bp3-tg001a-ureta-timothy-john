#pragma once
#include "Item.h"

class RareItem : public Item
{
public:
	RareItem(string name, int rollChance, int pullCost, int rarityPoint);
	~RareItem();

protected:
	int mRarityPoint = 0;
};

