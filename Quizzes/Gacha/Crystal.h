#pragma once
#include "Item.h"

class Crystal : public Item
{
	// Not to be confused with the crystal currency, although it does give the player more of that 
public:
	Crystal(string name, int rollChance, int pullCost, int valueCrystal);
	~Crystal();

	void executeEffect(Player *player);
	Item* clone();

	// Accessor function
	void incrementCounter(Player* player);

private:
	int mValueCrystal;
};

