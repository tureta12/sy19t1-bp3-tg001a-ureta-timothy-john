#include "Bomb.h"

Bomb::Bomb(string name, int rollChance, int pullCost, int damage) : Item(name, rollChance, pullCost)
{
	mDamage = damage;
}

Bomb::~Bomb()
{
}

void Bomb::executeEffect(Player *player)
{
	// Deal damage to the player
	cout << "You took " << mDamage << " damage!" << endl;
	player->takeDamage(mDamage);

}

Item* Bomb::clone()
{
	return new Bomb(*this);
}

void Bomb::incrementCounter(Player* player)
{
	player->incrementBombCounter();
}
