#pragma once
#include "GachaPool.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Player
{
public:
	Player(string name);
	~Player();

	// Actions
	void Pull(GachaPool* pool);

	// Accessor methods
	void takeDamage(int value);
	void heal(int value);
	void reduceCrystals(int value);
	void addCrystals(int value);
	void addRarityPoint(int value);
	void incrementHealthPotionCounter();
	void incrementBombCounter();
	void incrementCrystalCounter();
	void incrementRCounter();
	void incrementSRCounter();
	void incrementSSRCounter();

	// Getter methods
	string getName();
	int getRarityPoints();
	vector<Item*> getPlayerPulledItems();

	// others
	void printStats();
	void printPulledItems(); // The 10 points
	void printCollatedItems(); // The 15 points
	bool canPull();
	bool isAlive();

private:
	string mName;
	int mHP; // Player starts at 100 HP
	int mCrystals; // Player starts at 100 Crystals
	int mPulls;
	int mRarityPoints;

	// Counter variables for the items
	int mHealthPotionCounter;
	int mBombCounter;
	int mCrystalCounter;
	int mRCounter;
	int mSRCounter;
	int mSSRCounter;

	vector<Item*> mPulledItems; 
};

