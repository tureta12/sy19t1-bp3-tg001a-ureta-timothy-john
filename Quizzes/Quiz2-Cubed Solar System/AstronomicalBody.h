#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

class AstronomicalBody
{
public:
	AstronomicalBody(SceneNode* node);
	static AstronomicalBody* createBody(SceneManager& sceneManager, float size, ColourValue color);
	~AstronomicalBody();

	void update(const FrameEvent & evt);
	void translate(Vector3 input);


	SceneNode& getNode(); 
	void setParent(AstronomicalBody* parent);
	AstronomicalBody* getParent();

	void setLocalRoatationSpeed(float speed);
	void setRevolutionSpeed(float speed);

private:
	AstronomicalBody* mParent;
	SceneNode* mNode;
	Degree mRotationSpeed;
	Degree mRevolutionSpeed;
};

