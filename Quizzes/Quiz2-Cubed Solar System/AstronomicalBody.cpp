#include "AstronomicalBody.h"

AstronomicalBody::AstronomicalBody(SceneNode* node)
{
	mNode = node;
	mParent = NULL;
	mRotationSpeed = 0;
	mRevolutionSpeed = 0;
}

AstronomicalBody* AstronomicalBody::createBody(SceneManager& sceneManager, float size, ColourValue colour)
{
	AstronomicalBody* newBody = new AstronomicalBody(sceneManager.getRootSceneNode()->createChildSceneNode());
	ManualObject* astronomicalBody = sceneManager.createManualObject();
	astronomicalBody->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Face1
	astronomicalBody->position(-(size/2), (size/2), (size/2)); 	astronomicalBody->colour(colour);  astronomicalBody->position(-(size/2), -(size/2), (size/2)); astronomicalBody->position((size/2), -(size/2), (size/2));
	astronomicalBody->position((size/2), -(size/2), (size/2)); astronomicalBody->position((size/2), (size/2), (size/2)); astronomicalBody->position(-(size/2), (size/2), (size/2));

	// Face2
	astronomicalBody->position((size/2), (size/2), -(size/2));  astronomicalBody->position((size/2), -(size/2), -(size/2)); astronomicalBody->position(-(size/2), -(size/2), -(size/2));
	astronomicalBody->position(-(size / 2), -(size / 2), -(size / 2)); astronomicalBody->position(-(size / 2), (size / 2), -(size / 2)); astronomicalBody->position((size / 2), (size / 2), -(size / 2));

	// Face3
	astronomicalBody->position((size/2), (size/2), (size/2)); 	astronomicalBody->position((size/2), -(size/2), (size/2)); astronomicalBody->position((size/2), -(size/2), -(size/2));
	astronomicalBody->position((size/2), -(size/2), -(size/2));	astronomicalBody->position((size/2), (size/2), -(size/2)); astronomicalBody->position((size/2), (size/2), (size/2));

	// Face4
	astronomicalBody->position(-(size/2), (size/2), -(size/2));	astronomicalBody->position(-(size/2), -(size/2), -(size/2)); astronomicalBody->position(-(size/2), -(size/2), (size/2));
	astronomicalBody->position(-(size/2), -(size/2), (size/2)); astronomicalBody->position(-(size/2), (size/2), (size/2)); astronomicalBody->position(-(size/2), (size/2), -(size/2));

	// Face5
	astronomicalBody->position(-(size/2), (size/2), (size/2)); 	astronomicalBody->position((size/2), (size/2), (size/2)); astronomicalBody->position((size/2), (size/2), -(size/2));
	astronomicalBody->position((size/2), (size/2), -(size/2));  astronomicalBody->position(-(size/2), (size/2), -(size/2)); astronomicalBody->position(-(size/2), (size/2), (size/2));

	// Face 6
	astronomicalBody->position((size/2), -(size/2), -(size/2));  astronomicalBody->position((size/2), -(size/2), (size/2)); astronomicalBody->position(-(size/2), -(size/2), (size/2));
	astronomicalBody->position(-(size/2), -(size/2), (size/2));  astronomicalBody->position(-(size/2), -(size/2), -(size/2)); astronomicalBody->position((size/2), -(size/2), -(size/2));

	astronomicalBody->end();
	newBody->mNode->attachObject(astronomicalBody);

	return newBody;
}

AstronomicalBody::~AstronomicalBody()
{
}

void AstronomicalBody::update(const FrameEvent& evt)
{
	// Rotation
	mNode->rotate(Vector3(0, 1, 0), Radian(mRotationSpeed * evt.timeSinceLastFrame));

	// Revolution
	if (mParent != NULL)
	{
		Radian speed = Radian(mRevolutionSpeed * evt.timeSinceLastFrame);
		float oldX = (mNode->getPosition().x - getParent()->getNode().getPosition().x);
		float oldZ = (mNode->getPosition().z - getParent()->getNode().getPosition().z);
		float newX = (oldX*Math::Cos(speed)) + (oldZ*Math::Sin(speed));
		float newZ = (oldX*-Math::Sin(speed)) + (oldZ*Math::Cos(speed));
	
	mNode->setPosition(getParent()->getNode().getPosition().x + newX, mNode->getPosition().y,getParent()->getNode().getPosition().z + newZ); // I found out that getting the x and z position of the parent is important in terms of displacement such as for the moon.
	}
}

void AstronomicalBody::translate(Vector3 input)
{
	mNode->translate(input.x, input.y, input.z);
}

SceneNode& AstronomicalBody::getNode()
{
	return *mNode;
}

void AstronomicalBody::setParent(AstronomicalBody* parent)
{
	mParent = parent;
}

AstronomicalBody* AstronomicalBody::getParent()
{
	return mParent;
}

void AstronomicalBody::setLocalRoatationSpeed(float speed)
{
	mRotationSpeed = Degree(speed);
}

void AstronomicalBody::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = Degree(speed);
}

