/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include <vector>
#include "TutorialApplication.h"
// #include "AstronomicalBody.h" // Or forward this?
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------

void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	AstronomicalBody *justCreatedBody;

	// SUN
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 20, ColourValue::Red);
	justCreatedBody->setLocalRoatationSpeed(0.5);
	justCreatedBody->setRevolutionSpeed(0);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(0, 0, 0));

	// MERCURY
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 3, ColourValue::White);
	justCreatedBody->setLocalRoatationSpeed(12);
	justCreatedBody->setRevolutionSpeed(24.882); //4.147 * 6    (88 days / 365 days) * (6 degrees [since 365 days = 1 minute = 1 revolution = 360 degrees & 1 second is 6 degrees])
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(50, 0, 0));

	// VENUS
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 5, ColourValue::Green);
	justCreatedBody->setLocalRoatationSpeed(4.12);
	justCreatedBody->setRevolutionSpeed(9.774); //1.629 * 6
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(80, 0, 0));

	// EARTH
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 10, ColourValue::Blue);
	justCreatedBody->setLocalRoatationSpeed(3.89);
	justCreatedBody->setRevolutionSpeed(6);
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(110, 0, 0));

	// MOON
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 1, ColourValue::White);
	justCreatedBody->setLocalRoatationSpeed(20);
	justCreatedBody->setRevolutionSpeed(300);
	justCreatedBody->setParent(astronomicalBodies[3]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(125, 0, 0));

	// MARS
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 8, ColourValue::Red);
	justCreatedBody->setLocalRoatationSpeed(4.11);
	justCreatedBody->setRevolutionSpeed(3.186); //0.531 * 6
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(160, 0, 0));
;
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	for (int i = 0; i < astronomicalBodies.size(); i++)
		astronomicalBodies[i]->update(evt);

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
