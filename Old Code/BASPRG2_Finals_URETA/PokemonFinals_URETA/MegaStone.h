#pragma once
#include <string>

using namespace std;

class MegaStone
{
public:
	MegaStone(string, string, int, int);

	string name;
	string compatiblePokemonName;
	int count;
	int price;
};