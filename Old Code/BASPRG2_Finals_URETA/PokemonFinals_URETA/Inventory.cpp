#pragma once
#include "Inventory.h"

Inventory::Inventory()
{
	ItemList* itemsList = new ItemList();

	this->playerPokeBalls = itemsList->pokeballList;
	this->playerMegaStones = itemsList->megStoneList;
	this->playerRareCandies = itemsList->rareCandy;
	this->playerRareCandies->count = 20;
}

void Inventory::displayInventoryCategories()
{
	cout << "[1] PokeBalls     [2] MegaStones   [3] Rare Candies    [0] Exit\n" << endl;
}

void Inventory::displayplayerPokeBalls() //inventory outside of battle purposes
{
	for (unsigned int i = 0; i < playerPokeBalls.size(); i++)
	{
		if (playerPokeBalls[i]->count != 0)
		{
			cout << playerPokeBalls[i]->name << " " << playerPokeBalls[i]->count << "x" << endl;
		}
	}
	cout << endl;
}

void Inventory::displayplayerMegaStones() //inventory outside of battle purposes
{
	for (unsigned int i = 0; i < playerMegaStones.size(); i++)
	{
		if (playerMegaStones[i]->count != 0)
		{
			cout << playerMegaStones[i]->name << " " << playerMegaStones[i]->count << "x" << endl;
		}
	}
	cout << endl;
}

void Inventory::displayplayerPokeBallsBattle()
{
	int k = 1;
	for (unsigned int i = 0; i < playerPokeBalls.size(); i++)
	{
		cout << "[" << k << "]" << playerPokeBalls[i]->name << " " << playerPokeBalls[i]->count << "x" << endl;
		k++;
	}
	cout << endl << "[0] Exit" << endl;
}

void Inventory::displayplayerMegaStonesBattle()
{
	int j = 1;
	for (unsigned int i = 0; i < playerMegaStones.size(); i++)
	{
		cout << "[" << j << "]" << playerMegaStones[i]->name << " " << playerMegaStones[i]->count << "x" << endl;
		j++;
	}
	cout << endl << "[0] Exit" << endl;
}

void Inventory::displayplayerPokeBallsPrice()
{
	int k = 1;
	for (unsigned int i = 0; i < playerPokeBalls.size(); i++)
	{
		cout << "[" << k << "]" << playerPokeBalls[i]->name << ": " << playerPokeBalls[i]->price << " Poke" << endl;
		k++;
	}
	cout << endl << "[0] Exit" << endl;
}

void Inventory::displayplayerMegaStonesPrice()
{
	int j = 1;
	for (unsigned int i = 0; i < playerMegaStones.size(); i++)
	{
		cout << "[" << j << "]" << playerMegaStones[i]->name << ": " << playerMegaStones[i]->price << " Poke" << endl;
		j++;
	}
	cout << endl << "[0] Exit" << endl;
}


