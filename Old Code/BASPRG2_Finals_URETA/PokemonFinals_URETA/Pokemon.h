#pragma once
#include <string>
#include <iostream>
#include <vector>

using namespace std;

enum pokemonType
{
	Water, Fire, Grass, Electric, Ground, Rock, Dragon, Ice, Bug, Flying, Normal, Poison, Psychic, Ghost, Fighting, Dark, Steel
};

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string name, float baseHP, int level, float baseDamage, float baseExpToNextLevel, float givenXP, pokemonType type, int levelToEvolve, Pokemon* evolvedVersion);


	//CLASS VARIABLES
	string name;
	float baseHP; // maxHP for the level
	float currentHP; // currentHP
	int level;
	float baseDamage;
	float currentDamage;

	float exp;
	float baseExpToNextLevel;

	float givenXP; //xp given by a defeated pokemon

	int levelToEvolve;

	bool megaEvolved = false;

	pokemonType type;
	Pokemon* evolvedVersion; //for evolution


	//FUNCTIONS
	void levelUp(float);
	void Evolve(/*Pokemon*/);
	void evolutionCheck();
	//void megaEvolve(Pokemon*);

	void printType();
	void displayStats();

	void attack(Pokemon*);
	void applyBonusDmg(Pokemon*);

};