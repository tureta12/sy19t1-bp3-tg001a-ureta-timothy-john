#pragma once
#include <string>
#include <iostream>
#include "ItemList.h"

class Inventory
{
public:
	Inventory();

	vector<MegaStone*> playerMegaStones; //a vector of pointers   //then taking the address of each one.
	vector<Pokeball*> playerPokeBalls;
	RareCandy* playerRareCandies;

	void displayInventoryCategories();

	//outisde battle
	void displayplayerPokeBalls();
	void displayplayerMegaStones();

	//in battle
	void displayplayerPokeBallsBattle();
	void displayplayerMegaStonesBattle();

	//pokemart
	void displayplayerPokeBallsPrice();
	void displayplayerMegaStonesPrice();

	//uhm rare candy for testing? // LATER
//this could be another class, called a consumable. do this later
};