#pragma once
#include <string>
#include <vector>

class Location
{
public:
	Location(std::string, bool, std::vector<int>, std::vector<int>, std::vector<int>, std::vector<int>);

	std::string name;
	bool hasPokemonCenter;

	std::vector<int> topLeftCoordinates;
	std::vector<int>bottomLeftCoordinates;
	std::vector<int>topRightCoordinates;
	std::vector<int>bottomRightCoordinates;
};