#pragma once
#include "Pokeball.h"

Pokeball::Pokeball(string name, int catchChance, int price, int count)
{
	this->name = name;
	this->catchChance = catchChance;
	this->price = price;
	this->count = count;
}
