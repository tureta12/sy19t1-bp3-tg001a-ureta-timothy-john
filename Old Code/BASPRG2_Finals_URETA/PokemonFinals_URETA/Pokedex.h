#pragma once
#include "Pokemon.h"
#include <time.h>

class Pokedex
{
public:
	Pokedex();
	vector<Pokemon*> PokemonList;
	vector<Pokemon*> PokemonSpawnList;

	Pokemon* spawnPokemon();
};