#pragma once
#pragma once
#include <string>

using namespace std;

class Pokeball
{
public:

	Pokeball(string, int, int, int);

	string name;
	int catchChance;
	int price;
	int count;
};
