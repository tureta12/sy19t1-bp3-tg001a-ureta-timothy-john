#include "Player.h"
#include "Skill.h"

void getValidInput(int& input)
{
	// Loop till input is valid
	while (true)
	{
		cin >> input;
		if (cin.fail())
		{
			cout << "\nInvalid input. Please try again." << endl;
			cin.clear();
			cin.ignore();
		}

		else
		{
			break;
		}
	}
}

Player::Player(string name)
{
	mName = name;
	mIsOutOfUnits = false;
}

Player::~Player()
{
	// delete the vectors
	for (int i = 0; i < mTeam.size(); i++)
	{
		delete mTeam[i];
	}
	mTeam.clear();
}

Skill* Player::chooseSkill(Unit* currentUnit)
{
	int input = 0;
	cout << "\nChoose a skill to use: ";

	while (true)
	{
		getValidInput(input);
		input--;

		// Account for inputting a number well beyond the size of the skillList vector
		if (input <= currentUnit->getSkillList().size() - 1)
		{
			// If the unit doesn�t have enough MP. Let the user choose again.
			if (currentUnit->getMP() < currentUnit->getSkillList()[input]->getMPCost())
			{
				cout << "Not enough mana to use that Skill, please choose another one." << endl;
			}
			else
				return currentUnit->getSkillList()[input];
		}

		else
		{
			cout << "Please input a valid choice." << endl;
		}
	}

}

Unit* Player::pickTarget(Skill* currentSkill)
{
	int input = 0;
	vector<Unit*> availableTargets;

	// For healing, availableTargets will be caster's team
	if (currentSkill->getName() == "Heal")
		availableTargets = this->getAliveTeam();
	// Else get opponent's team
	else 
		availableTargets = mOpponent->getAliveTeam();

	cout << "\nSelect a target." << endl;

	// Display available targets 
	for (int i = 0; i < availableTargets.size(); i++)
		cout << "[" << i + 1 << "] " << availableTargets[i]->getName() << " (" << availableTargets[i]->getHP() << "/" << availableTargets[i]->getMaxHP() << " HP) - " << availableTargets[i]->getSpecialization() << endl;

	while (true)
	{
		cout << "Target: ";
		getValidInput(input);
		input--;
		cout << "\n";

		// Account for inputting a number well beyond the size of the aliveTeam vector
		if (input <= availableTargets.size() - 1)
			return availableTargets[input];
		else
			cout << "Please input a valid target." << endl;
	}
}

void Player::updateAliveUnits(Unit* deadUnit)
{
	int indexOfDeadUnit = 0;
	// Remove from mAliveUnits and add to mDeadUnits if there is someone with 0 HP
	// Cycle through 
	for (int i = 0; i < mAliveUnits.size(); i++)
		if (deadUnit->getName() == mAliveUnits[i]->getName())
			indexOfDeadUnit = i;
	Unit* temp = mAliveUnits[indexOfDeadUnit];
	mAliveUnits.erase(mAliveUnits.begin() + indexOfDeadUnit);
	mDeadUnits.push_back(temp);
}

void Player::setOpponent(Player* opponent)
{
	mOpponent = opponent;
}

void Player::setPlayerOutOfUnitsToTrue()
{
	mIsOutOfUnits = true;
}

void Player::addUnit(Unit* unit)
{
	mTeam.push_back(unit);
	mAliveUnits.push_back(unit);
}

string Player::getName()
{
	return mName;
}

vector<Unit*> Player::getTeam()
{
	return mTeam;
}

vector<Unit*> Player::getAliveTeam()
{
	return mAliveUnits;
}

Player* Player::getOpponent()
{
	return mOpponent;
}

bool Player::isPlayerOutOfUnits()
{
	return mIsOutOfUnits;
}
