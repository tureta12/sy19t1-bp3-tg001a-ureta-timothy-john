#pragma once
#include <string>
#include <time.h>

using namespace std;

//forward declarations
class Player; 
class Unit;

class Skill
{
public:
	Skill(string name, string description, string type, int mpCost, float damageCoefficient);
	~Skill();

	virtual void cast(Unit* caster, vector<Unit*> target); // For multi target
	virtual void cast(Unit* caster, Unit* target); // For single target

	// Getter methods
	float getDamageCoefficient();
	int getMPCost();
	string getName();
	string getDescription();
	string getType();

protected:
	string mName;
	string mType;
	string mDescription;
	int mMpCost;
	float mDamageCoefficient;
};

