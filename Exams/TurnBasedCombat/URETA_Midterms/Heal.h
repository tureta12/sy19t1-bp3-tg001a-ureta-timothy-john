#pragma once
#include <vector>
#include "Skill.h"

using namespace std;

class Heal : public Skill
{
public:
	Heal(string name, string description, string type, int mpCost, float damageCoefficient);
	~Heal();

	void cast(Unit* caster, Unit* target);

};

