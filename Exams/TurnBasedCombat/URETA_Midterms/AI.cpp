#include "AI.h"
#include "Skill.h"

AI::AI(string name) : Player(name)
{
	mIsOutOfUnits = false;
}

AI::~AI()
{
}

Skill* AI::chooseSkill(Unit* currentUnit)
{
	// Just output a message and pause for feedback to user
	cout << "\n" << this->getName() << " is calculating..." << endl;
	system("pause");

	// Computer chooses a random skill
	// On enemy�s turn, there should be a 50 % chance to use skill given that he has enough MP.
	int randomNumber = 0;

	randomNumber = rand() % (currentUnit->getSkillList().size()) + 1; // Since there are only two skills, this is already 50%
	randomNumber--;

	// AI should not use a skill if it doesn�t have enough MP.
	if (currentUnit->getMP() < currentUnit->getSkillList()[randomNumber]->getMPCost())
	{
		randomNumber = 0; // Index for basic attacks
	}

	else if(currentUnit->getSkillList()[randomNumber]->getName() == "Heal")
	{
		// Behavior for Healing Skill. Only use if an ally has received damage
		bool noDamage = true;
		for (int i = 0; i < currentUnit->getOwner()->getAliveTeam().size(); i++)
			if (currentUnit->getOwner()->getAliveTeam()[i]->getHP() < currentUnit->getOwner()->getAliveTeam()[i]->getMaxHP())
				noDamage = false;

		if (noDamage == true)
			randomNumber = 0; // The index for all basic attacks
	}
	return currentUnit->getSkillList()[randomNumber];
}

Unit* AI::pickTarget(Skill* currentSkill)
{
	cout << endl; 	// Spacing
	vector<Unit*> availableTargets;
	
	// For healing, availableTargets will be caster's team
	if (currentSkill->getName() == "Heal")
	{
		// Heals 30 % of Max HP of the allied unit with the lowest HP
		// Cycle through allied vector of units till requirements are met, then designate that as the target
		availableTargets = this->getAliveTeam();
		Unit* target = availableTargets[0];

		// Variables for position
		int i = 0, j = 1;
		for (int m = 0; m < availableTargets.size() - 1; m++)
		{
			int hpDifference = availableTargets[i]->getMaxHP() - availableTargets[i]->getHP();
			int hpDifferenceNext = availableTargets[j]->getMaxHP() - availableTargets[j]->getHP();

			// Since the units all have different HP, the idea here is to get the difference and choose the unit with the biggest difference in HP 
			if (hpDifference > hpDifferenceNext)
			{
				target = availableTargets[i];
			}

			else
			{
				target = availableTargets[j];
				i = j;
			}
			j++;
		}
		return target;
	}

	// Else get opponent's team
	else
	{
		availableTargets = mOpponent->getAliveTeam();

		// Randomization of target
		int randomNumber = rand() % (availableTargets.size()) + 1;
		randomNumber--;
		return availableTargets[randomNumber];
	}
}

