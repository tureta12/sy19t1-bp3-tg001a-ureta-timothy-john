#pragma once
#include "Player.h"

class Skill;

class AI : public Player
{
public:
	AI(string name);
	~AI();

	// ACTIONS
	Skill* chooseSkill(Unit* currentUnit);
	Unit* pickTarget(Skill* currentSkill);
};

