#include <iostream>
#include <time.h>
#include <vector>
#include "characterGenerator.h"
#include "AI.h"
#include "Assassinate.h"
#include "BasicAttack.h"
#include "Heal.h"
#include "Skill.h"
#include "Shockwave.h"

using namespace std;

void randomizeStat(int& stat, int max, int min)
{
	int range;
	range = max - min + 1; // Do i need this +1? I think that it will prevent the random number from not starting at 0s
	stat = rand() % range + min;
}

void getValidInput(int& input); // Forwarding

void generateRandomizedCharacter(int& mMaxHP, int& mMaxMP, int& mPOW, int& mVIT, int& mAGI, int& mDEX, int& classNameIndex, int& randomNameIndex, string& className, string& unitName, vector<string> classNamePool, vector<vector<string>> unitNamePool)
{
	// A page printing randomized stats of a hypothetical character including if it's assassin etc.
	// The way this works is that it depends on the classNameIndex to know whether it's making an "Assassin" or "Warrior" or "Mage" 

	className = classNamePool[classNameIndex];

	randomizeStat(mMaxHP, 300, 170);
	randomizeStat(mMaxMP, 75, 40);
	randomizeStat(mPOW, 110, 80);
	randomizeStat(mVIT, 50, 25);
	randomizeStat(mAGI, 50, 25);
	randomizeStat(mDEX, 50, 25);

	randomNameIndex = rand() % (unitNamePool[classNameIndex].size() - 1);
	unitName = unitNamePool[classNameIndex][randomNameIndex];
}

void insertSkillsAndSetStrongAgainst(int classNameIndex, Unit* unit, vector<string> classNamePool)
{
	// Set which specialization is the unit strong against 
	// Create new objects of the skills and add them to the skillList vector of the Unit 
	// I could also probably have made a class called skillList which has member vectors like assassinSkills or mageSkills and then I just copy from there 

	if (classNameIndex == 0) // For Assassins
	{
		BasicAttack* stab = new BasicAttack("Stab", "A basic stab with the use of the Assassin's dagger.", "Single Target", 0, 1.0);
		Assassinate* assassinate = new Assassinate("Assassinate", "The Assassin emerges from the shadows and deals high damage to a target.", "Single Target", 30, 1.8);
		unit->addSkill(stab);
		unit->addSkill(assassinate);
		unit->setStrongAgainst(classNamePool[2]); // Strong against a mage
	}

	else if (classNameIndex == 1) // For Warriors
	{
		BasicAttack* slash = new BasicAttack("Slash", "The Warrior slashes their sword with power and grace.", "Single Target", 0, 1.0);
		Shockwave* shockwave = new Shockwave("Shockwave", "The very ground is split apart by The Warrior's raw power.", "AOE", 20, 0.9);
		unit->addSkill(slash);
		unit->addSkill(shockwave);
		unit->setStrongAgainst(classNamePool[0]); // Strong against an assassin
	}

	else if (classNameIndex == 2) // For Mages
	{
		BasicAttack* fireball = new BasicAttack("Fireball", "A simple spell that is powerful in the right hands.", "Single Target", 0, 1.0);
		Heal* heal = new Heal("Heal", "The Mage calls upon the spirits and heals the member in their team with the lowest HP.", "Single Target", 20, 0.0);
		unit->addSkill(fireball);
		unit->addSkill(heal);
		unit->setStrongAgainst(classNamePool[1]); // Strong against a warrior
	}
}

characterGenerator::characterGenerator()
{
	mClassPool = { "ASSASSIN", "WARRIOR", "MAGE" };
	mAssassinNamePool = { "Quickshot", "Redeye", "Hollow Mark", "Crimson Sign", "Crimsonscar" };
	mWarriorNamePool = { "Wildhide", "The Rebel", "Demonblow", "Phoenixblood", "Colossus" };
	mMageNamePool = { "Aldor", "Mavior", "Imolleas", "Gisior", "Istrea" };
	mUnitNamePool = { mAssassinNamePool, mWarriorNamePool, mMageNamePool };
	
	mCurrentUnits = 0;
	mMaxUnits = 3; // Fixed max size of 3 
	mClassNameIndex = 0; // To know whether to make a warrior, an assassin or a mage
	mRandomNameIndex = 0; // To know where the index of the name that is used is 
}

characterGenerator::~characterGenerator()
{
}

void characterGenerator::createPlayerUnits(Player* player)
{
	// Resetting variables for use in player unit creation
	mCurrentUnits = 0;
	mClassNameIndex = 0;

	while (mCurrentUnits < mMaxUnits)
	{
		// Generate a random character and display it's stats. No object is created yet
		generateRandomizedCharacter(mMaxHP, mMaxMP, mPOW, mVIT, mAGI, mDEX, mClassNameIndex, mRandomNameIndex, mClassName, mUnitName, mClassPool, mUnitNamePool);

		// Display the generated stats
		cout << "HERO STATS:" << endl;
		cout << "Name: " << mUnitName << endl;
		cout << "Class: " << mClassName << endl;
		cout << "HP: " << mMaxHP << endl;
		cout << "MP: " << mMaxMP << endl;
		cout << "POW: " << mPOW << endl;
		cout << "VIT: " << mVIT << endl;
		cout << "AGI: " << mAGI << endl;
		cout << "DEX: " << mDEX << endl;

		// Player will be asked if they are satisfied with the roll. If they are good with it, then it's added to their team.
		cout << "\nWould you like to hire this Hero?" << endl;
		cout << "[1] Yes		[2] No" << endl;
		cout << "\nChoice: ";
		getValidInput(mHireChoice);
		if (mHireChoice == 1)
		{
			// Create an object when the player hires the Hero and set ownership
			Unit* createdUnit = new Unit(mUnitName, mClassPool[mClassNameIndex], mMaxHP, mMaxMP, mPOW, mVIT, mAGI, mDEX);
			createdUnit->setOwner(player);

			// Give the unit the appropriate skills according to its specialization
			insertSkillsAndSetStrongAgainst(mClassNameIndex, createdUnit, mClassPool);
			player->addUnit(createdUnit);
			cout << "\n" << createdUnit->getName() << " has been added to your team!\n" << endl;

			// Remove the used name from its corresponding vector
			mUnitNamePool[mClassNameIndex].erase(mUnitNamePool[mClassNameIndex].begin() + mRandomNameIndex);
			mClassNameIndex++; // Then increment so that we will be generating random stats of the next class on the array 
			mCurrentUnits++; // Increment # of units when hired 
		}
		else
		{
			// If not, then they can keep on rolling stats per specialization until they are satisfied. 
			cout << "\nHero wasn't hired.\n" << endl;
		}
		system("pause");
		system("cls");
	}
}

void characterGenerator::createAIUnits(AI* computer)
{
	// Resetting variables for use in AI unit creation
	mCurrentUnits = 0;
	mClassNameIndex = 0;

	while (mCurrentUnits < mMaxUnits)
	{
		generateRandomizedCharacter(mMaxHP, mMaxMP, mPOW, mVIT, mAGI, mDEX, mClassNameIndex, mRandomNameIndex, mClassName, mUnitName, mClassPool, mUnitNamePool);
		Unit* createdUnit = new Unit(mUnitName, mClassPool[mClassNameIndex], mMaxHP, mMaxMP, mPOW, mVIT, mAGI, mDEX);
		createdUnit->setOwner(computer);

		// Give the unit the appropriate skills according to its specialization
		insertSkillsAndSetStrongAgainst(mClassNameIndex, createdUnit, mClassPool);

		// Using a setter method, push back the unit to the player instance team
		computer->addUnit(createdUnit);

		// Remove the used name from its corresponding vector
		mUnitNamePool[mClassNameIndex].erase(mUnitNamePool[mClassNameIndex].begin() + mRandomNameIndex);
		mClassNameIndex++; // Then increment so that we will be generating random stats of the next class on the array 
		mCurrentUnits++; // Increment once created
	}
	system("cls");
}
