#pragma once
#include <iostream>
#include <string>
#include <vector>

class Skill;
class Player;

using namespace std;

class Unit
{
public:
	Unit(string name, string specialization, int HP, int MP, int POW, int VIT, int AGI, int DEX);
	~Unit();

	void useSKill(int index, Unit* target);

	// Accessor methods 
	void takeDamage(int value);
	void takeHealing(int value);
	void reduceMP(int value);
	void setUnitToDead();
	void computeDamage(Skill *skill, Unit *target);
	void addCriticalDamage(int value);
	void computeHitRate(Unit* target);
	void setStrongAgainst(string specialization);
	void setOwner(Player* player);
	void addSkill(Skill* skill);

	// Getter methods
	int getHP();
	int getMaxHP();
	int getMP();
	int getMaxMP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();
	int getDamage();
	int getHitRate();
	string getName();
	string getSpecialization();
	vector<Skill*> getSkillList();
	Player* getOwner();

	// Other
	void printStats(); 
	void printSkills();

private:
	string mName;
	string mStrongAgainst; // Specialization name of a unit that it is strong against 
	string mSpecialization; // Warrior, Assassin or Mage
	vector<Skill*> mSkillList;
	Player* mOwner;

	int mHP;
	int mMaxHP;
	int mMP;
	int mMaxMP;
	int mPOW;
	int mVIT;
	int mAGI;
	int mDEX;
	int mDamage; // (POW of attacker - VIT of defender) * bonus damage if strong against enemy specialization	20% VARIANCE ON POW (RANDOM)
	int mHitRate; // (DEX of attacker / AGI of defender) * 100. Hit chance never be lower than 20 and higher than 80
	bool mIsAlive = true;
};

