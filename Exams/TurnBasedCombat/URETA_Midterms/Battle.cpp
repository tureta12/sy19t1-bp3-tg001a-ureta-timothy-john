#include "Battle.h"
#include "Skill.h"

Battle::Battle(Player* human, Player* computer)
{
	mPlayer = human;
	mAI = computer;

	// Set opponent
	human->setOpponent(computer);
	computer->setOpponent(human);

	// Push back human and computer units into mTurnOrder vector 
	for (int i = 0; i < human->getAliveTeam().size(); i++)
	{
		mTurnOrder.push_back(human->getAliveTeam()[i]);
	}

	for (int i = 0; i < computer->getAliveTeam().size(); i++)
	{
		mTurnOrder.push_back(computer->getAliveTeam()[i]);
	}
}

Battle::~Battle()
{
	// Clear the turnorder vector. just clear don't delete. let the player and AI class deconstructor do the deletion 
	mTurnOrder.clear();
}

void Battle::startBattle()
{
	int input;
	Unit* currentUnit;
	vector<Unit*> targetTeam;
	Skill* chosenSkill;
	Player* opponent;
	sortTurnOrder();

	// Battle sequence loop
	while ((mPlayer->isPlayerOutOfUnits() == false) && (mAI->isPlayerOutOfUnits() == false))
	{
		displayTurnOrder();
		cout << endl;
		currentUnit = mTurnOrder[0];
		opponent = currentUnit->getOwner()->getOpponent();
		targetTeam = opponent->getAliveTeam();

		// Print stats of current unit
		currentUnit->printStats();
		currentUnit->printSkills();

		// Ask player to choose a skill from the current unit's skillList
		chosenSkill = currentUnit->getOwner()->chooseSkill(currentUnit);

		if (chosenSkill->getType() == "AOE") // If shockwave the target is the whole team
			chosenSkill->cast(currentUnit, targetTeam);
		else
			chosenSkill->cast(currentUnit, currentUnit->getOwner()->pickTarget(chosenSkill));

		// Cycle through the targetTeam
		for (int i = 0; i < targetTeam.size(); i++)
		{
			// If there is a death
			if (targetTeam[i]->getHP() <= 0)
			{
				cout << targetTeam[i]->getName() << " has died." << endl;
				// Remove that unit from the turn order and from the aliveUnit of player object
				removeDeadUnitFromOrder(targetTeam[i]);

				// Define again because we updated targetTeam? If i don't do this the program bugs out and it doesn't read the updated size 
				opponent->updateAliveUnits(targetTeam[i]);
				targetTeam = opponent->getAliveTeam();
			}
		}
	
		// Move current unit who had just made an attack to the last part of the turn order
		moveCurrentToLast();

		// I placed the check after the code block which modifies the aliveUnit vector of the opponent of the owner of the current unit
		if (targetTeam.size() <= 0)
		{
			opponent->setPlayerOutOfUnitsToTrue(); // This will break the loop
		}

		system("pause");
		system("cls");
	}

	// Print out who won 
	if (mPlayer->isPlayerOutOfUnits())
		cout << mPlayer->getName() << " is out of units, " << mAI->getName() << " won the battle!" << endl;
	else
		cout << mAI->getName() << " is out of units, " << mPlayer->getName() << " won the battle!" << endl;
	system("pause");
}

void Battle::sortTurnOrder()
{
	// Sort according to stats (AGI). Highest goes first.
	// Bubble Sort
	for (int i = 0; i < mTurnOrder.size() - 1; i++)
	{
		bool swapped = false;
		for (int j = 0; j < mTurnOrder.size() - 1 - i; j++)
		{
			if (mTurnOrder[j]->getAGI() < mTurnOrder[j + 1]->getAGI())
			{
				Unit* temp = mTurnOrder[j];
				mTurnOrder[j] = mTurnOrder[j + 1];
				mTurnOrder[j + 1] = temp;
				swapped = true;
			}
		 }
		if (swapped == false)
			break;
	}
}

void Battle::moveCurrentToLast()
{
	// Once a unit has finished it's turn, put it in a temp
	Unit* temp = mTurnOrder[0];

	// Then erase it, vector will automatically resize
	mTurnOrder.erase(mTurnOrder.begin());

	// Then insert the temp at the end 
	mTurnOrder.push_back(temp);
}

void Battle::removeDeadUnitFromOrder(Unit* deadUnit)
{
	// If a unit died, remove from the Turn Order
	// Find it's position in the turn order and then remove it 
	for (int i = 0; i < mTurnOrder.size(); i++)
	{
		if (mTurnOrder[i]->getName() == deadUnit->getName())
		{
			mTurnOrder.erase(mTurnOrder.begin() + i);
			break;
		}
	}
}

void Battle::displayTurnOrder()
{
	cout << "Turn Order: " << endl;
	for (int i = 0; i < mTurnOrder.size(); i++)
	{
		cout << "[" << i + 1 << "] " << mTurnOrder[i]->getName() << "(AGI " << mTurnOrder[i]->getAGI() << ") - " << mTurnOrder[i]->getOwner()->getName() << endl;
	}
}
