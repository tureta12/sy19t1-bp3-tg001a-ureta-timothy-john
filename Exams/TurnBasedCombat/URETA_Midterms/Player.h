#pragma once
#include "Unit.h"
#include <string>
#include <vector>

using namespace std;

class Skill;

class Player
{
public:
	Player(string name);
	~Player();

	virtual Skill* chooseSkill(Unit* currentUnit);
	virtual Unit* pickTarget(Skill* currentSKill);

	// Accessor method
	void updateAliveUnits(Unit* deadUnit); // puts dead untis in the mDeadUnits vector and also removes said units in the Battle turnorder vector
	void setOpponent(Player* opponent);
	void setPlayerOutOfUnitsToTrue();
	void addUnit(Unit* unit);

	// Getter method
	string getName();
	vector<Unit*> getTeam();
	vector<Unit*> getAliveTeam(); 
	Player* getOpponent();
	bool isPlayerOutOfUnits();

	// Other
	void printTeam(); // prints out the stats one by one 

protected:
	string mName;
	vector<Unit*> mTeam;
	vector<Unit*> mAliveUnits;
	vector<Unit*> mDeadUnits;
	bool mIsOutOfUnits;
	Player* mOpponent;
};

