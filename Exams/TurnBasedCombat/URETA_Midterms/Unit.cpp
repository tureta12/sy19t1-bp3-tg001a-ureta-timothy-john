#include "Unit.h"
#include "Skill.h"

Unit::Unit(string name, string specialization, int HP, int MP, int POW, int VIT, int AGI, int DEX)
{
	mName = name;
	mSpecialization = specialization;
	mMaxHP = HP;
	mHP = mMaxHP;
	mMaxMP = MP;
	mMP = mMaxMP;
	mPOW = POW;
	mVIT = VIT;
	mAGI = AGI;
	mDEX = DEX;
}

Unit::~Unit()
{
	// Delete Skills
	for (int i = 0; i < mSkillList.size(); i++)
	{
		delete mSkillList[i];
	}
	mSkillList.clear();
}

void Unit::useSKill(int index, Unit* target)
{
	mSkillList[index]->cast(this, target);
}

void Unit::takeDamage(int value)
{
	cout << this->mName << " received " << value << " damage!" << endl;
	this->mHP -= value;
}

void Unit::takeHealing(int value)
{
	if (mHP == mMaxHP)
		cout << "This hero is already at full HP!" << endl;
	else
	{
		cout << this->mName << " has been healed for " << value << " HP!" << endl;
		mHP += value;

		// In case mHP goes above maxHP
		if (mHP > mMaxHP)
			mHP = mMaxHP;
	}
}

void Unit::reduceMP(int value)
{
	mMP -= value;
}

void Unit::setUnitToDead()
{
	mIsAlive = false;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getMaxHP()
{
	return mMaxHP;
}

int Unit::getMP()
{
	return mMP;
}

int Unit::getMaxMP()
{
	return mMaxMP;
}

int Unit::getPOW()
{
	return mPOW;
}

int Unit::getVIT()
{
	return mVIT;;
}

int Unit::getAGI()
{
	return mAGI;
}

int Unit::getDEX()
{
	return mDEX;
}

void Unit::computeDamage(Skill *skill, Unit *target)
{
	// Randomed Power has a variance of 20% of the unit�s Power. This means if your POW is 100, 
	// If POW is 100. Randomed POW should range from 100-119. This is because 20% of 100 is 20. POW determines the lowest base damage a unit can achieve.
	int variancePOW = 20;
	int randomedPOW = mPOW + (rand() % (variancePOW - 1));

	// BaseDamage = Randomed POW * damageCoefficient
	int baseDamage = float(randomedPOW) * (skill->getDamageCoefficient());

	// Damage = (baseDamage of attacker - VIT of defender) * bonusDamage
	mDamage = baseDamage- target->getVIT();

	// Remember that bonus damage is only applicable if the attacker is strong against the defender. 
	// Also, bonus damage is always 150%
	float bonusDamageMultiplier = 1.5;
	if (mStrongAgainst == target->getSpecialization())
	{
		cout << this->getName() << " is strong against " << target->getName() << "!" << endl;
		cout << this->getName() << "'s damage has been increased by " << bonusDamageMultiplier << "x!\n" << endl;
		mDamage = float(mDamage) * bonusDamageMultiplier;
	}

    // The lowest damage you can deal is 1.
	int floorDamage = 1;
	if (mDamage < floorDamage)
		mDamage = floorDamage;
}

void Unit::addCriticalDamage(int value)
{
	mDamage *= float(value);
}

void Unit::computeHitRate(Unit * target)
{
	mHitRate = (((float)mDEX / (float)target->getAGI()) * 100.0f);

	// hit% can never be lower than 20 and higher than 80. This means you need to clamp the result within this range.
	int ceilingHitRate = 80;
	int floorHitRate = 20;
	if (mHitRate < floorHitRate)
		mHitRate = floorHitRate;
	if (mHitRate > ceilingHitRate)
		mHitRate = ceilingHitRate;
}

void Unit::setStrongAgainst(string specialization)
{
	mStrongAgainst = specialization;
}

void Unit::setOwner(Player* player)
{
	mOwner = player;
}

void Unit::addSkill(Skill* skill)
{
	mSkillList.push_back(skill);
}

int Unit::getDamage()
{
	return mDamage;;
}

int Unit::getHitRate()
{
	return mHitRate;
}

string Unit::getName()
{
	return mName;
}

string Unit::getSpecialization()
{
	return mSpecialization;
}

vector<Skill*> Unit::getSkillList()
{
	return mSkillList;
}

Player* Unit::getOwner()
{
	return mOwner;
}

void Unit::printStats()
{
	cout << "CURRENT HERO Stats:" << endl;
	cout << "Name: " << mName << endl;
	cout << "Class: " << mSpecialization << endl;
	cout << "HP: " << mHP << "/" << mMaxHP << endl;
	cout << "MP: " << mMP << "/" << mMaxMP << endl;
	cout << "POW: " << mPOW << endl;
	cout << "VIT: " << mVIT << endl;
	cout << "AGI: " << mAGI << endl;
	cout << "DEX: " << mDEX << endl;
}

void Unit::printSkills()
{
	cout << "\nSkills: " << endl;
	for (int i = 0; i < mSkillList.size(); i++)
		cout << "[" << i + 1 << "]" << mSkillList[i]->getName() << " (" << mSkillList[i]->getMPCost() << " MP)" << " - " << mSkillList[i]->getDescription() <<  " (" << mSkillList[i]->getType() << ")" << endl;
}
