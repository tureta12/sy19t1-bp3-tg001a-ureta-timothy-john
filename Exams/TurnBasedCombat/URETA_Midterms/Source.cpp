#include <iostream>
#include <string>
#include <time.h>
#include "Battle.h"
#include "characterGenerator.h"

using namespace std;

void getValidInput(int& input); // Forwarded. Already defined in Player.cpp

int main()
{
	string playerNameInput;
	int battleAgainChoice;
	srand(time(NULL));

	cout << "Welcome to the arena! What's your name? " << endl;
	cin >> playerNameInput; 

	// Create the generator object
	characterGenerator* charGen = new characterGenerator();

	while (true)
	{
		// Create the player objects. They are inside the while loop because they are deleted at the end of each battle. If player wishes to battle again then new instances will be made 
		Player* player = new Player(playerNameInput);
		AI* computer = new AI("JustAnAI");

		cout << "Alright " << playerNameInput << ", time to pick your team!" << endl;
		system("pause");

		// Generate units
		charGen->createAIUnits(computer);
		charGen->createPlayerUnits(player);

		cout << "Looks like you're ready " << playerNameInput << ", it's time to command your team and fight against " << computer->getName() << "!" << endl;
		system("pause");
		cout << endl;

		// Make new battle class and start it 
		Battle* battle = new Battle(player, computer);
		battle->startBattle();

		// Deletion
		delete player;
		delete computer;

		cout << "Would you like to battle with again with a different team?" << endl;
		cout << "[1] Yes     [2] No" << endl;
		cout << "Choice: ";
		getValidInput(battleAgainChoice);

		if (battleAgainChoice != 1)
		{
			// If no, end the loop
			cout << "Thanks for playing!" << endl;
			break;
		}
	}

	// Deletion
	delete charGen;

	system("pause");
	return 0;
}
