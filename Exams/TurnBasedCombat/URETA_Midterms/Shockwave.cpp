#include "Player.h"
#include "Unit.h"
#include "Shockwave.h"

Shockwave::Shockwave(string name, string description, string type, int mpCost, float damageCoefficient) : Skill(name, description, type, mpCost, damageCoefficient)
{
}

Shockwave::~Shockwave()
{
}

void Shockwave::cast(Unit * caster, vector<Unit*> target)
{
	// Skill usage message
	cout << caster->getName() << " used " << this->getName() << "!\n" << endl;

	// Deals damage to all opposing units. Randomed base damage must be different for each target. (randomed base damage is done in the compute damage function)
	// Cycle through the target vector of units, 
	for (int i = 0; i < target.size(); i++)
	{
		caster->computeDamage(this, target[i]);
		target[i]->takeDamage(caster->getDamage());
		if (i != target.size() - 1)
			system("pause");
		cout << endl;
	}

	// Reduce caster's MP
	caster->reduceMP(this->getMPCost());
}
