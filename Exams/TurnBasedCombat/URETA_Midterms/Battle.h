#pragma once
#include "AI.h"

//class Units;
class Skill;

class Battle
{
public:
	Battle(Player* human, Player* computer);
	~Battle();

	void startBattle();

	// Accessor method
	void sortTurnOrder();
	void moveCurrentToLast();
	void removeDeadUnitFromOrder(Unit* deadUnit);

	// Other
	void displayTurnOrder();

private:
	Player* mPlayer;
	Player* mAI;

	vector<Unit*> mTurnOrder; // contains all the untis. we will need an accessor function to rearrange this turn order if someone is dead, maybe something that takes from the player class
	// since there are already methods there which deal with dead units. 

};

