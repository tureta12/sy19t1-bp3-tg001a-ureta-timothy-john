#pragma once
#include "Unit.h"

class Player;
class AI;

class characterGenerator
{
public:
	characterGenerator();
	~characterGenerator();

	void createPlayerUnits(Player* player);
	void createAIUnits(AI* computer);

private:
	int mMaxHP;
	int mMaxMP;
	int mPOW;
	int mVIT;
	int mAGI;
	int mDEX;
	string mUnitName;
	string mClassName;
	vector<string> mClassPool; 

	// these vectors are for the randomization of names 
	vector<string> mAssassinNamePool;
	vector<string> mWarriorNamePool;
	vector<string> mMageNamePool;
	vector<vector<string>> mUnitNamePool;

	int mCurrentUnits;
	int mMaxUnits;
	int mHireChoice;
	int mClassNameIndex; // To know whether to make a warrior, an assassin or a mage
	int mRandomNameIndex; // To know where the index of the name that is used is 
};

