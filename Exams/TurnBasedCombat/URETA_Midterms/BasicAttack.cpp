#include "Player.h"
#include "Unit.h"
#include "BasicAttack.h"

BasicAttack::BasicAttack(string name, string description, string type, int mpCost, float damageCoefficient) : Skill(name, description, type, mpCost, damageCoefficient)
{
}

BasicAttack::~BasicAttack()
{
}

void BasicAttack::cast(Unit* caster, Unit * target)
{
	// Output skill used message
	cout << caster->getName() << " used " << this->mName << " on " << target->getName() << "!" << endl;

	// Compute for the damage
	caster->computeDamage(this, target);

	// Can crit. 20% chance to deal 20% bonus damage so we take the cmoputed damage and multiply that by 1.2
	int random = rand() % 100 + 1;
	if (random <= 20)
		caster->addCriticalDamage(1.2f);

	// Facilitate miss chance 
	caster->computeHitRate(target);
	int attackChanceRoll = rand() % 100 + 1;
	if (attackChanceRoll <= caster->getHitRate())
	{
		// Execute takeDamage() 
		target->takeDamage(caster->getDamage());
	}

	else
	{
		cout << caster->getName() << " missed!" << endl;
	}
}
