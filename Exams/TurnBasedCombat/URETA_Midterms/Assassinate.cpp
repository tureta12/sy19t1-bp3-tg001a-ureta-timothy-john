#include "Player.h"
#include "Unit.h"
#include "Assassinate.h"

Assassinate::Assassinate(string name, string description, string type, int mpCost, float damageCoefficient) : Skill(name, description, type, mpCost, damageCoefficient)
{
}

Assassinate::~Assassinate()
{
}

void Assassinate::cast(Unit * caster, Unit * target)
{
	// Skill used message
	cout << caster->getName() << " used " << this->getName() << " on " << target->getName() << "!" << endl;

	// Compute for the damage
	caster->computeDamage(this, target);

	// Execute takeDamage()
	target->takeDamage(caster->getDamage());

	// Reduce MP
	caster->reduceMP(this->getMPCost());
}
