#pragma once
#include "Skill.h"

class Assassinate : public Skill
{
public:
	Assassinate(string name, string description, string type, int mpCost, float damageCoefficient);
	~Assassinate();

	void cast(Unit* caster, Unit* target);
};

