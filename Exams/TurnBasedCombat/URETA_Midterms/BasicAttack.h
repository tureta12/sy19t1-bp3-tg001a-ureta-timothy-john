#pragma once
#include "Skill.h"

class BasicAttack : public Skill
{
public:
	BasicAttack(string name, string description, string type, int mpCost, float damageCoefficient);
	~BasicAttack();

	void cast(Unit* caster, Unit* target);


private:
	
};

