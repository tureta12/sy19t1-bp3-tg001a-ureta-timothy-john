#include "Player.h"
#include "Unit.h"
#include "Skill.h"

Skill::Skill(string name, string description, string type, int mpCost, float damageCoefficient)
{
	mName = name;
	mDescription = description;
	mType = type;
	mMpCost = mpCost;
	mDamageCoefficient = damageCoefficient;
}

Skill::~Skill()
{
}

void Skill::cast(Unit* caster, vector<Unit*> target)
{
}

void Skill::cast(Unit* caster, Unit* target)
{
}

float Skill::getDamageCoefficient()
{
	return mDamageCoefficient;
}

int Skill::getMPCost()
{
	return mMpCost;
}

string Skill::getName()
{
	return mName;
}

string Skill::getDescription()
{
	return mDescription;
}

string Skill::getType()
{
	return mType;
}
