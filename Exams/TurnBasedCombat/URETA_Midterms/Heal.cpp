#include "Player.h"
#include "Unit.h"
#include "Heal.h"
#include <vector>

using namespace std;

Heal::Heal(string name, string description, string type, int mpCost, float damageCoefficient) : Skill(name, description, type, mpCost, damageCoefficient)
{
}

Heal::~Heal()
{
}

void Heal::cast(Unit* caster, Unit* target)
{
	// Heals 30 % of Max HP of the targeted allied unit
	// Get the heal value and output message, then execute takeHealing function
	int healValue = target->getMaxHP() * 0.3f;
	cout << caster->getName() << " used " << this->getName() << " on " << target->getName() << "!" << endl;
	target->takeHealing(healValue);

	// Reduce caster's mana
	caster->reduceMP(this->mMpCost);
}
