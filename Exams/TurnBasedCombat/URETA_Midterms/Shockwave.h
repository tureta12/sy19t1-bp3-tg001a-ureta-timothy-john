#pragma once
#include <vector>
#include "Skill.h"

using namespace std;

class Shockwave : public Skill
{
public:
	Shockwave(string name, string description, string type, int mpCost, float damageCoefficient);
	~Shockwave();

	void cast(Unit* caster, vector<Unit*> target);
};

