#include "AstronomicalBody.h"

AstronomicalBody::AstronomicalBody(SceneNode* node)
{
	mNode = node;
	mParent = NULL;
	mRotationSpeed = 0;
	mRevolutionSpeed = 0;
}

AstronomicalBody::~AstronomicalBody()
{
}

AstronomicalBody* AstronomicalBody::createBody(SceneManager& sceneManager, float size, ColourValue colour, std::string material, std::vector<float> diffuseParameters, std::vector<float> specularParameters)
{
	AstronomicalBody* newBody = new AstronomicalBody(sceneManager.getRootSceneNode()->createChildSceneNode());
	ManualObject* manual = sceneManager.createManualObject();

	if (material == "sunMat") // For Sun
	{
		manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	}

	else
	{
		// NOTE: The second parameter to the create method is the resource group the material will be added to.
		// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
		newBody->myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(material, "General");
		newBody->myManualObjectMaterial->setReceiveShadows(false);
		newBody->myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
		newBody->myManualObjectMaterial->getTechnique(0)->setDiffuse(diffuseParameters[0], diffuseParameters[1], diffuseParameters[2], diffuseParameters[3]);
		newBody->myManualObjectMaterial->getTechnique(0)->setSpecular(specularParameters[0], specularParameters[1], specularParameters[2], specularParameters[3]);

		manual->begin(material, RenderOperation::OT_TRIANGLE_LIST);
	}

	// Store the vertices in a vector first 
	std::vector<Vector3> vertices; 

	// Add the first vertex at 0,0
	vertices.push_back(Vector3::ZERO);

	// Compute for the vertices first
		 // Resource: http://wiki.ogre3d.org/ManualSphereMeshes
	// Resolution
	int stackHeight = 32;
	int numSegments = 32;

	float deltaRingAngle = (Math::PI / stackHeight); // The change in vertical distance between rings
	float anglePerPoint = (2 * Math::PI / numSegments); //using Math::PI so that we won't have to convert to Radian later on
	float radius = size / 2;

	for (int stack = 0; stack <= stackHeight; stack++) //STACK
	{
		float xOfStack = radius * Math::Sin(stack * deltaRingAngle);
		float y = radius * Math::Cos(stack * deltaRingAngle);

		for (int j = 0; j <= numSegments; j++) //SLICE
		{
			float x = xOfStack * Math::Sin(j * anglePerPoint);
			float z = xOfStack * Math::Cos(j * anglePerPoint);
			Vector3 vert(x, y, z);
			manual->position(vert);
			manual->colour(colour);

			// Normals (normalized vertex position)
			manual->normal(vert.normalisedCopy());
			vertices.push_back(vert);
		}
	}

	// Plot indices
	for (int j = 0; j < vertices.size(); j++)
	{
		manual->index(j);
		manual->index(j + numSegments);
		manual->index(j + numSegments + 1);
		manual->index(j + numSegments + 1);
		manual->index(j + 1);
		manual->index(j);
	}

	 //Loop back for last triangle
	manual->index(vertices.size() - 1);
	manual->index(1);
	manual->index(0);

	manual->end();
	newBody->mNode->attachObject(manual);
	return newBody;
}


void AstronomicalBody::update(const FrameEvent& evt)
{
	// Rotation
	mNode->rotate(Vector3(0, 1, 0), Radian(mRotationSpeed * evt.timeSinceLastFrame));

	// Revolution
	if (mParent != NULL)
	{
		Radian speed = Radian(mRevolutionSpeed * evt.timeSinceLastFrame);
		float oldX = (mNode->getPosition().x - getParent()->getNode().getPosition().x);
		float oldZ = (mNode->getPosition().z - getParent()->getNode().getPosition().z);
		float newX = (oldX * Math::Cos(speed)) + (oldZ * Math::Sin(speed));
		float newZ = (oldX * -Math::Sin(speed)) + (oldZ * Math::Cos(speed));

		mNode->setPosition(getParent()->getNode().getPosition().x + newX, mNode->getPosition().y, getParent()->getNode().getPosition().z + newZ); // I found out that getting the x and z position of the parent is important in terms of displacement such as for the moon.
	}
}

void AstronomicalBody::translate(Vector3 input)
{
	mNode->translate(input.x, input.y, input.z);
}

SceneNode& AstronomicalBody::getNode()
{
	return *mNode;
}

void AstronomicalBody::setParent(AstronomicalBody* parent)
{
	mParent = parent;
}

AstronomicalBody* AstronomicalBody::getParent()
{
	return mParent;
}

void AstronomicalBody::setLocalRoatationSpeed(float speed)
{
	mRotationSpeed = Degree(speed);
}

void AstronomicalBody::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = Degree(speed);
}

