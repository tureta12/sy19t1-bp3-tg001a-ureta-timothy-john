/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include <vector>
#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------

void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	AstronomicalBody* justCreatedBody;

	// SUN
	std::vector<float> diffuseParameters = { 0, 0, 0, 0.0}; // (RED, GREEN, BLUE, ALPHA)
	std::vector<float> specularParameters = { 0, 0, 0, 0.0}; // (RED, GREEN, BLUE, SHININESS)
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 20, ColourValue(1, 1, 0), "sunMat", diffuseParameters, specularParameters);
	justCreatedBody->setLocalRoatationSpeed(1);
	justCreatedBody->setRevolutionSpeed(0);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->getNode().setPosition(Vector3(0, 0, 0));

	// MERCURY
	diffuseParameters = {0.7, 0.7, 0.54, 0.0};
	specularParameters = {0.92, 0.7, 0.54, 0.4};
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 3, ColourValue(0.82f, 0.7f, 0.54f), "mercuryMat", diffuseParameters, specularParameters);
	justCreatedBody->setLocalRoatationSpeed(12);
	justCreatedBody->setRevolutionSpeed(24.882); //4.147 * 6    (1/ (88 days / 365 days)) * (6 degrees [since 365 days = 1 minute = 1 revolution = 360 degrees so 1 second is 6 degrees])
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(40, 0, 0));

	// VENUS
	diffuseParameters = { 0.82, 0.76, 0.5, 0.0 };
	specularParameters = { 0.83, 0.3, 0.6, 5.5 };
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 5, ColourValue(0.93f, 0.9f, 0.57f), "venusMat", diffuseParameters, specularParameters);
	justCreatedBody->setLocalRoatationSpeed(4.12);
	justCreatedBody->setRevolutionSpeed(9.774); //1.629 * 6
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(65, 0, 0));

	// EARTH
	diffuseParameters = { 0.0, 0.0, 10, 0.0};
	specularParameters = { 0.001, 0.1, 1, 50 };
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 10, ColourValue::Blue, "earthMat", diffuseParameters, specularParameters);
	justCreatedBody->setLocalRoatationSpeed(3.89);
	justCreatedBody->setRevolutionSpeed(6);
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(90, 0, 0));

	// MOON
	diffuseParameters = { 0.7, 0.7, 0.7, 0.0 };
	specularParameters = { 0.6, 0.7, 0.6, 0.5 };
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 1, ColourValue(0.9f, 0.9f, 0.9f), "moonMat", diffuseParameters, specularParameters);
	justCreatedBody->setLocalRoatationSpeed(20);
	justCreatedBody->setRevolutionSpeed(300);
	justCreatedBody->setParent(astronomicalBodies[3]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(105, 0, 0));

	// MARS
	diffuseParameters = { 0.99, 0.25, 0.2, 0.0 };
	specularParameters = { 0.89, 0.2, 0.3, 1.3 };
	justCreatedBody = AstronomicalBody::createBody(*mSceneMgr, 8, ColourValue(0.71f, 0.25f, 0.05f), "marsMat", diffuseParameters, specularParameters);
	justCreatedBody->setLocalRoatationSpeed(4.11);
	justCreatedBody->setRevolutionSpeed(3.186); //0.531 * 6
	justCreatedBody->setParent(astronomicalBodies[0]);
	astronomicalBodies.push_back(justCreatedBody);
	astronomicalBodies.back()->translate(Vector3(120, 0, 0));
	
	// LIGHTING CODE FROM SIR 
	// Lecture: Adding a light source
	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 0.8f, 0.1));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 0.0f));
	// Values taken from: http://www.ogre3d.org/tikiwiki/-Point+Light+Attenuation
	pointLight->setAttenuation(600, 1.0, 0.007, 0.0002); // 325, 0.0, 0.014, 0.0007
	pointLight->setCastShadows(false);

	// Lecture: Setting ambient light
	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	for (int i = 0; i < astronomicalBodies.size(); i++)
		astronomicalBodies[i]->update(evt);

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
