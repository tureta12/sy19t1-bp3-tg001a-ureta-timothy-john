/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------


void createCube(ManualObject* object, float size)
{
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Face1
	object->position(-size, size, size); 	object->colour(0, 1, 1);  object->position(-size, -size, size); object->colour(0, 2, 1); object->position(size, -size, size);
	object->position(size, -size, size);	object->colour(1, 0, 0);  object->position(size, size, size); object->colour(0, 2, 1); object->position(-size, size, size);

	// Face2
	object->position(size, size, -size); 	object->colour(0, 1, 0);  object->position(size, -size, -size); object->colour(0, 2, 1); object->position(-size, -size, -size);
	object->position(-size, -size, -size);	object->colour(0, 0, 1);  object->position(-size, size, -size); object->colour(0, 2, 1); object->position(size, size, -size);

	// Face3
	object->position(size, size, size); 	object->colour(1, 0, 1);  object->position(size, -size, size); object->colour(0, 2, 1); object->position(size, -size, -size);
	object->position(size, -size, -size);	object->colour(0, 0, 1);  object->position(size, size, -size); object->colour(0, 2, 1); object->position(size, size, size);

	// Face4
	object->position(-size, size, -size);	object->colour(1, 0, 1); object->position(-size, -size, -size); object->colour(0, 2, 1); object->position(-size, -size, size);
	object->position(-size, -size, size);  	object->colour(0, 1, 1); object->position(-size, size, size); object->colour(0, 2, 1); object->position(-size, size, -size);

	// Face5
	object->position(-size, size, size); 	object->colour(1, 1, 0);  object->position(size, size, size); object->colour(0, 2, 1); object->position(size, size, -size);
	object->position(size, size, -size); 	object->colour(1, 0, 0);  object->position(-size, size, -size); object->colour(0, 2, 1); object->position(-size, size, size);

	// Face 6
	object->position(size, -size, -size); 	object->colour(1, 0, 0);  object->position(size, -size, size); object->colour(0, 2, 1); object->position(-size, -size, size);
	object->position(-size, -size, size); 	object->colour(0, 1, 0);  object->position(-size, -size, -size); object->colour(0, 2, 1); object->position(size, -size, -size);

	object->end();
}

void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	ManualObject* cube = mSceneMgr->createManualObject("object1");
	createCube(cube, 10);
	mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(cube);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		mSpeedY += mAcceleration * evt.timeSinceLastFrame;
		mKeyIsPressed = true;
	}

	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
	   mSpeedY -= mAcceleration * evt.timeSinceLastFrame;
	   mKeyIsPressed = true;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		mSpeedX -= mAcceleration * evt.timeSinceLastFrame;
		mKeyIsPressed = true;
	}

	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		mSpeedX += mAcceleration * evt.timeSinceLastFrame;
		mKeyIsPressed = true;
	}

	if (mKeyIsPressed != true)
	{
		// Reset speed
		mSpeedX = mSetSpeed;
		mSpeedY = mSetSpeed;
	}

	mKeyIsPressed = false;
	mNode->translate(mSpeedX * evt.timeSinceLastFrame, mSpeedY * evt.timeSinceLastFrame, mSpeedZ * evt.timeSinceLastFrame); 
	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
