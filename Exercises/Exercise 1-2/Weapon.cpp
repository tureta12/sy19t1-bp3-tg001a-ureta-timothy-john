#include "Weapon.h"

Weapon::Weapon(string name, int baseDamage)
{
	this->name = name;
	this->baseDamage = baseDamage;
}

Weapon::~Weapon()
{
}

string Weapon::getWeaponName()
{
	return this->name;
}

int Weapon::getWeaponBaseDamage()
{
	return this->baseDamage;
}
