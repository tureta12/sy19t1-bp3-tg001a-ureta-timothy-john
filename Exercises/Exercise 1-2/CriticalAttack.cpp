#include "CriticalAttack.h"
#include "Character.h"

CriticalAttack::CriticalAttack(string name)
{
	this->name = name;
}

CriticalAttack::~CriticalAttack()
{
}

void CriticalAttack::cast(Character* caster, Character* target)
{
	cout << "Critical Strike attempt!" << endl;
	int roll = rand() % 100 + 1;
	if (roll <= 20)
	{
		cout << "A Critical Strike! Bonus Damage 2x" << endl;
		caster->modifyCharacterDamage(caster->getCharacterDamage() * 2);
	}

	else
	{
		cout << "Attempt failed! Normal damage will be dealt." << endl;
	}
	system("pause");
}