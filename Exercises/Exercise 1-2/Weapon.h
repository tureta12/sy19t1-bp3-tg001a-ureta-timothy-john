#pragma once
#include <string>

using namespace std;

class Weapon
{
public:
	Weapon(string name, int baseDamage);
	~Weapon();

	string getWeaponName();
	int getWeaponBaseDamage();

private:
	string name;
	int baseDamage;
};

