#include "Vengeance.h"
#include "Character.h"

Vengeance::Vengeance(string name)
{
	this->name = name;
	this->mpCost = 10;
}

Vengeance::~Vengeance()
{
}

void Vengeance::cast(Character* caster, Character* target)
{
	this->hpCost = caster->getCurrentHP() / 4;
	int damageFromHP = (caster->getCurrentHP() / 4) * 2;
	cout <<  caster->getName() << " unleashed Vengeance!" << endl;
	cout << this->mpCost << " MP has been consumed." << endl;
	cout << caster->getName() << " has sacrificed " << this->hpCost << " HP!" << endl;
	cout << "Damage has been increased!" << endl;
	system("pause");
	cout << endl;

	caster->modifyCharacterDamage(caster->getCharacterDamage() + damageFromHP);
	caster->modifyCurrentCharacterHP(-(this->hpCost));
	caster->modifyCurrentCharacterMP(-(this->mpCost));

	if (caster->getCurrentHP() < 0)
	{
		caster->modifyCurrentCharacterHP(caster->getCurrentHP() - caster->getCurrentHP() + 1); //get to 0 first then add 1 point of HP.
	}
}
