#include "DopelBlade.h"
#include "Character.h"

DopelBlade::DopelBlade(string name)
{
	this->name = name;
	this->mpCost = 10;
}

DopelBlade::~DopelBlade()
{

}

void DopelBlade::cast(Character* caster, Character* target)
{
	cout << "Dopel Blade was used!" << endl;
	cout << this->mpCost << " MP has been consumed." << endl;
	cout << "A clone was created and attacked!" << endl;
	system("pause");

	caster->modifyCurrentCharacterMP(-this->mpCost); // modify mana before cloning 

	//PROBLEM EARLIER WAS THAT IT WAS DELETING THE ORIGINAL'S WEAPON. so i first have to make a copy of the WEAPON????
	Weapon* clonedWeapon = new Weapon(caster->getWeapon()->getWeaponName(), caster->getWeapon()->getWeaponBaseDamage());
	//cout << "ClonedWeapon created" << endl; system("pause"); // for Testing
	Character* clone = new Character(caster->getName() + "'s Clone", caster->getCurrentHP(), caster->getCurrentMP(), clonedWeapon);
	//cout << "clone created" << endl; system("pause");// for Testing
	clone->cloneCounter++; 
	//cout << "Clone count incremented" << endl; system("pause");// for Testing
	clone->attack(target);
	//cout << "Deleting clone" << endl; system("pause");// for Testing
	delete clone;
	//cout << "clone deleted" << endl; system("pause");// for Testing
}