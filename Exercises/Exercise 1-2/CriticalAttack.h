#pragma once
#include "Skill.h"

class CriticalAttack : public Skill
{
public:
	CriticalAttack(string name);
	~CriticalAttack();

	void cast(Character* caster, Character* target);
};

