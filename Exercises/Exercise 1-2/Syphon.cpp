#include "Syphon.h"
#include "Character.h"

Syphon::Syphon(string name)
{
	this->name = name;
	this->mpCost = 15;
}

Syphon::~Syphon()
{
}

void Syphon::cast(Character* caster, Character* target)
{
	// code for syphon assumes that the caster isn't healed by an instance of syphon casted by a clone 
	this->hpCost = caster->getCharacterDamage();
	cout << "Syphon was used!" << endl;
	cout << this->mpCost << " MP has been consumed." << endl;
	cout << caster->getName() << " has siphoned " << this->hpCost << " points of health." << endl;
	system("pause");

	caster->modifyCurrentCharacterHP(this->hpCost);
	caster->modifyCurrentCharacterMP(-this->mpCost);
}