
#include "Character.h"

#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	//object initializing
	Weapon* royalSword = new Weapon("Royal Sword", 20);
	Weapon* greatsword = new Weapon("Great Sword", 20);

	Character* player = new Character("Player", 100, 50, royalSword);
	Character* enemy = new Character("The Enemy", 100, 50, greatsword);

	cout << player->getName() << " is the first one to attack!" << endl;
	system("pause");
	system("cls");

	//main game loop
	while (true)
	{
		cout << player->getName() << "'s turn!" << endl;
		system("pause");
		cout << endl;

		player->printStats();
		enemy->printStats();

		if (player->getCurrentHP() <= 0)
		{
			cout << enemy->getName() << " is victorious!" << endl;
			break;
		}

		system("pause");
		player->attack(enemy);
		cout << endl;
		system("pause");

		system("cls");
		cout << enemy->getName() << "'s turn!" << endl;
		system("pause");

		player->printStats();
		enemy->printStats();
		system("pause");

		if (enemy->getCurrentHP() <= 0)
		{
			cout << player->getName() << " is victorious!" << endl;
			break;
		}

		enemy->attack(player);
		cout << endl;

		system("pause");
		system("cls");
	}

	cout << endl;
	cout << "The game has ended" << endl;
	system("pause");

	//deleting
	return 0;
	delete player;
	delete enemy;
	delete royalSword;
	delete greatsword;

}