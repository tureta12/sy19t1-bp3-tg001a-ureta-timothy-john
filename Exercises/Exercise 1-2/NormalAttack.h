#pragma once
#include "Skill.h"

class NormalAttack : public Skill
{
public:
	NormalAttack(string name);
	~NormalAttack();

	void cast(Character* caster, Character* target);
};

