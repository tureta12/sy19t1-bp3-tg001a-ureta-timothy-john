#pragma once
#include "Weapon.h"
#include "Skill.h"

#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

class Character
{
public:
	static int cloneCounter;

	Character(string name, int hp, int mp, Weapon* weapon);
	~Character();

	//actions
	void attack(Character* enemy);
	void printStats();

	//Getter functions
	string getName();
	Weapon* getWeapon();
	void modifyCharacterDamage(int newDamage);
	void modifyCurrentCharacterHP(int additionalHP);
	void modifyCurrentCharacterMP(int MP);
	int getCharacterDamage();
	int getCurrentHP();
	int getMaxHP();
	int getCurrentMP();
	int getMaxMP();

private:
	string name;
	int maxHP;
	int currentHP;
	int maxMP;
	int currentMP;
	int characterDamage;
	Weapon* weapon;
	vector<Skill*> skillList;
};

