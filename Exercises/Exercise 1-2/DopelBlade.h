#pragma once
#include "Skill.h"

class DopelBlade : public Skill
{
public:

	DopelBlade(string name);
	~DopelBlade();

	void cast(Character* caster, Character* target);
};


