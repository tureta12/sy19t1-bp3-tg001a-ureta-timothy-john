#pragma once
#include <iostream>
#include <string>

using namespace std;

class Character; //forward declaration

class Skill
{
public:
	Skill();
	~Skill();

	string getName();
	virtual void cast(Character* caster, Character* target);

	int getManaCost();

protected:
	string name;
	int mpCost = 0;
	int hpCost = 0;

};

