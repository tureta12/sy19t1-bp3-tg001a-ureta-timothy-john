#include "Character.h"
#include "Skill.h"

#include "NormalAttack.h"
#include "CriticalAttack.h"
#include "Syphon.h"
#include "Vengeance.h"
#include "DopelBlade.h"

#include <iostream>

using namespace std;

int Character::cloneCounter = 0; // static counter initialized

Character::Character(string name, int hp, int mp, Weapon* weapon)
{
	this->name = name;
	this->maxHP = hp;
	this->currentHP = hp;
	this->maxMP = mp;
	this->currentMP = mp;
	this->weapon = weapon;
	this->characterDamage = weapon->getWeaponBaseDamage();

	NormalAttack* normalAttack = new NormalAttack("Normal Attack");
	CriticalAttack* criticalAttack = new CriticalAttack("Critical Attack");
	Syphon* syphon = new Syphon("Syphon");
	Vengeance* vengeance = new Vengeance("Vengeance");
	DopelBlade* dopelBlade = new DopelBlade("Dopel Blade");

	skillList = { normalAttack, criticalAttack, syphon, vengeance, dopelBlade };
}

Character::~Character()
{
	delete weapon;  

	//delete skillList 
	for (int i = 0; i < skillList.size(); i++)
	{
		delete skillList[i];
	}
	// why? maybe because this vector is an array of POINTERS. and just clearing or deleting it just clears the POINTER and not the actualy object which is what we want to delete
	// after that we now do clear to CLEAR the POINTERS. what we did awhile ago was just to delete the objects. 
	skillList.clear();

	// the memory allocated for the vector itstelf is still here though
	// clearing the memory of the skillList vector
		// make a new empty vector called temp whatever and swap the contents of the current vector (which is now empty with it) to deallocate memory 
		// and apparently i won't have to loop through and clear the vector if i do only this?
	vector<Skill*>().swap(skillList);
}

void Character::attack(Character* enemy)
{
	Character* attacker = this;
	srand(time(NULL));
	int roll = rand() % skillList.size() + 1;
	Skill* rolledSkill = skillList[(roll - 1)];

	// check mana first
	if (attacker->currentMP < rolledSkill->getManaCost())
	{
		cout << attacker->getName() << " used " << rolledSkill->getName();
		cout << "Not enough mana! The attack failed." << endl;
	}

	else
	{
		rolledSkill->cast(attacker, enemy); 

		if (rolledSkill->getName() != "Dopel Blade") // dopel blade deals no damage since its basically just a chance to get another skill roll?
		{
			if (cloneCounter == 0) // if there are no clones 
			{
				//cout << "Executing normal attack function (no clone)." << endl; //TESTING. remove this in final.
				cout << attacker->getName() << " dealt " << attacker->characterDamage << " damage to " << enemy->getName() << "!" << endl;
				enemy->currentHP -= attacker->characterDamage;

				// reset the damage (especially on vengeance)
				attacker->characterDamage = attacker->getWeapon()->getWeaponBaseDamage(); 
			}

			else // this means that a clone used a skill other than dopel blade. so reset the clone counter cuz it means that the clone has attacked and done its job and is now gone
			{
				cloneCounter = 0;
				//cout << "Clone count resetted." << endl; // TESTING. remove this in final. 
				cout << attacker->getName() << " dealt " << attacker->characterDamage << " damage to " << enemy->getName() << "!" << endl;
				enemy->currentHP -= attacker->characterDamage; 
				//cout << "I've modified the target's HP!" << endl; // TESTING. remove in final
				// no need to reset the damage since the clone will be gone anyway
			}
		}
	}
}

void Character::printStats()
{
	cout << this->getName() << endl;
	cout << "HP: " << this->getCurrentHP() << "/" << this->getMaxHP() << endl;
	cout << "MP: " << this->getCurrentMP() << "/" << this->getMaxMP() << endl;
	cout << "Weapon: " << this->weapon->getWeaponName() << endl;
	cout << "Damage: " << this->characterDamage << endl << endl;
}

string Character::getName()
{
	return this->name;
}

Weapon* Character::getWeapon()
{
	return this->weapon;
}

void Character::modifyCharacterDamage(int newDamage)
{
	this->characterDamage = newDamage;
}

void Character::modifyCurrentCharacterHP(int additionalHP)
{
	this->currentHP += additionalHP;
}

void Character::modifyCurrentCharacterMP(int MP)
{
	this->currentMP += MP;
}

int Character::getCharacterDamage()
{
	return this->characterDamage;
}

int Character::getCurrentHP()
{
	return this->currentHP;
}

int Character::getMaxHP()
{
	return this->maxHP;
}

int Character::getCurrentMP()
{
	return this->currentMP;
}

int Character::getMaxMP()
{
	return this->maxMP;
}
