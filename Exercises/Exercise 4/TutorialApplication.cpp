/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	mRotation = 40;
	mCenter = (0, 0, 0);
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------


void createCube(ManualObject* object, float size)
{
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Face1
	object->position(-size, size, size); 	object->colour(0, 1, 1);  object->position(-size, -size, size); object->colour(0, 2, 1); object->position(size, -size, size);
	object->position(size, -size, size);	object->colour(1, 0, 0);  object->position(size, size, size); object->colour(0, 2, 1); object->position(-size, size, size);

	// Face2
	object->position(size, size, -size); 	object->colour(0, 1, 0);  object->position(size, -size, -size); object->colour(0, 2, 1); object->position(-size, -size, -size);
	object->position(-size, -size, -size);	object->colour(0, 0, 1);  object->position(-size, size, -size); object->colour(0, 2, 1); object->position(size, size, -size);

	// Face3
	object->position(size, size, size); 	object->colour(1, 0, 1);  object->position(size, -size, size); object->colour(0, 2, 1); object->position(size, -size, -size);
	object->position(size, -size, -size);	object->colour(0, 0, 1);  object->position(size, size, -size); object->colour(0, 2, 1); object->position(size, size, size);

	// Face4
	object->position(-size, size, -size);	object->colour(1, 0, 1); object->position(-size, -size, -size); object->colour(0, 2, 1); object->position(-size, -size, size);
	object->position(-size, -size, size);  	object->colour(0, 1, 1); object->position(-size, size, size); object->colour(0, 2, 1); object->position(-size, size, -size);

	// Face5
	object->position(-size, size, size); 	object->colour(1, 1, 0);  object->position(size, size, size); object->colour(0, 2, 1); object->position(size, size, -size);
	object->position(size, size, -size); 	object->colour(1, 0, 0);  object->position(-size, size, -size); object->colour(0, 2, 1); object->position(-size, size, size);

	// Face 6
	object->position(size, -size, -size); 	object->colour(1, 0, 0);  object->position(size, -size, size); object->colour(0, 2, 1); object->position(-size, -size, size);
	object->position(-size, -size, size); 	object->colour(0, 1, 0);  object->position(-size, -size, -size); object->colour(0, 2, 1); object->position(size, -size, -size);

	object->end();
}

void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	ManualObject* cube = mSceneMgr->createManualObject("object1");
	createCube(cube, 10);
	mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(cube);

	// EXERCISE 4 - 2
	mNode->translate(20, 0, 0);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	//// EXERCISE 4 - 1
	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
	//{
	//	mNode->rotate(Vector3(0, -1, 0), Radian(mRotation * evt.timeSinceLastFrame));
	//}

	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	//{
	//	mNode->rotate(Vector3(0, 1, 0), Radian(mRotation * evt.timeSinceLastFrame));
	//}

	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	//{
	//	mNode->rotate(Vector3(-1, 0, 0), Radian(mRotation * evt.timeSinceLastFrame));
	//}

	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	//{
	//	mNode->rotate(Vector3(1, 0, 0), Radian(mRotation * evt.timeSinceLastFrame));
	//}
	
	// EXERCISE 4 - 2
	Radian degreeRotation = Radian(mRotation * evt.timeSinceLastFrame);
	float oldX = (mNode->getPosition().x - mCenter.x);
	float oldZ = (mNode->getPosition().z - mCenter.z);
	float newX = (oldX * Math::Cos(degreeRotation)) + (oldZ * Math::Sin(degreeRotation));
	float newZ = (oldX * -Math::Sin(degreeRotation)) + (oldZ * Math::Cos(degreeRotation));
	mNode->setPosition(mCenter.x + newX, mNode->getPosition().y, mCenter.z + newZ);
	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
