#pragma once
#include "Skill.h"

class Vengeance : public Skill
{
public:
	Vengeance(string name);
	~Vengeance();

	void cast(Character * caster, Character * target);
};

