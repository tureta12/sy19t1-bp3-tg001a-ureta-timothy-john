#include "Character.h"
#include "Skill.h"

#include "NormalAttack.h"
#include "CriticalAttack.h"
#include "Syphon.h"
#include "Vengeance.h"
#include "DopelBlade.h"

#include <iostream>

using namespace std;

Character::Character(string name, int hp, int mp, Weapon * weapon)
{
	this->name = name;
	this->maxHP = hp;
	this->currentHP = hp;
	this->maxMP = mp;
	this->currentMP = mp;
	this->weapon = weapon;
	this->characterDamage = weapon->getWeaponBaseDamage();

	NormalAttack* normalAttack = new NormalAttack("Normal Attack");
	CriticalAttack* criticalAttack = new CriticalAttack("Critical Attack");
	Syphon* syphon = new Syphon("Syphon");
	Vengeance* vengeance = new Vengeance("Vengeance");
	DopelBlade* dopelBlade = new DopelBlade("Dopel Blade");

	skillList = { normalAttack, criticalAttack, syphon, vengeance, dopelBlade };
}

Character::~Character()
{
	delete this->weapon;
	//delete skillList; // how do i delete the new instsances of the skills i made. FOR LOOP delete one by one since it's basically uninitialized since they the construcotr and deconstructor are 
    //in the same .cpp 
	for (int i = 0; i < (skillList.size() - 1); i++)
	{
		delete skillList[i];
	}
	skillList.clear();
}

void Character::attack(Character * enemy)
{
	Character* attacker = this;
	srand(time(NULL));
	int roll = rand() % skillList.size() + 1;
	Skill* rolledSkill = skillList[(roll - 1)];

	//exception to Dopel clone
	if (rolledSkill->getName() == "Dopel Blade")
	{
		srand(time(NULL)); //in the hopes of resetting the seed so we won't get a double Dopel Dude??
	}

	rolledSkill->cast(attacker, enemy);
	//after modifying damage values and subtracting HP and MP if needed,
	//deal the damage

	if (rolledSkill->getName() != "Dopel Blade") //dopel blade deals no damage since its basically just a chance to get another skill roll?
	{
		cout << attacker->getName() << " dealt " << attacker->characterDamage << " damage to " << enemy->getName() << "!" << endl;
		enemy->currentHP -= attacker->characterDamage;

		//reset the damage
		attacker->characterDamage = attacker->getWeapon()->getWeaponBaseDamage();
	}
}

void Character::printStats()
{
	cout << this->getName() << endl;
	cout << "HP: " << this->getCurrentHP() << "/" << this->getMaxHP() << endl;
	cout << "MP: " << this->getCurrentMP() << "/" << this->getMaxMP() << endl;
	cout << "Weapon: " << this->weapon->getWeaponName() << endl;
	cout << "Damage: " << this->characterDamage << endl << endl;
}

string Character::getName()
{
	return this->name;
}

Weapon* Character::getWeapon()
{
	return this->weapon;
}

void Character::modifyCharacterDamage(int newDamage)
{
	this->characterDamage = newDamage;
}

void Character::modifyCurrentCharacterHP(int additionalHP)
{
	this->currentHP += additionalHP;
}

void Character::modifyCurrentCharacterMP(int MP)
{
	this->currentMP += MP;
}

int Character::getCharacterDamage()
{
	return this->characterDamage;
}

int Character::getCurrentHP()
{
	return this->currentHP;
}

int Character::getMaxHP()
{
	return this->maxHP;
}

int Character::getCurrentMP()
{
	return this->currentMP;
}

int Character::getMaxMP()
{
	return this->maxMP;
}
