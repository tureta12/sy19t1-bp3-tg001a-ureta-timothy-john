#include "Syphon.h"
#include "Character.h"

Syphon::Syphon(string name)
{
	this->name = name;
}

Syphon::~Syphon()
{
}

void Syphon::cast(Character * caster, Character * target)
{
	this->hpCost = caster->getCharacterDamage();
	cout << "Syphon was used!" << endl;
	cout << caster->getName() << " has siphoned " << this->hpCost << " points of health." << endl;
	system("pause");

	caster->modifyCurrentCharacterHP(this->hpCost);
}