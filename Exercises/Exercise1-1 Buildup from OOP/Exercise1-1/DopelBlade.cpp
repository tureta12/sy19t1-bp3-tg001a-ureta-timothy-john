#include "DopelBlade.h"
#include "Character.h"

DopelBlade::DopelBlade(string name)
{
	this->name = name;
	this->mpCost = 5;
}

DopelBlade::~DopelBlade()
{

}

void DopelBlade::cast(Character * caster, Character* target)
{
	cout << "Dopel Blade was used!" << endl;
	cout << "A clone was created and attacked!" << endl;
	system("pause");

	Character* clone = new Character(caster->getName() + "'s Clone", caster->getCurrentHP(), caster->getCurrentMP(), caster->getWeapon());
	caster->modifyCurrentCharacterMP(-this->mpCost);

	clone->attack(target);

	//how to delete the clone made
}