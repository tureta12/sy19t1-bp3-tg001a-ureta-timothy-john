#pragma once
#include "Skill.h"

class Syphon : public Skill
{
public:
	Syphon(string name);
	~Syphon();

	void cast(Character * caster, Character * target);
};

