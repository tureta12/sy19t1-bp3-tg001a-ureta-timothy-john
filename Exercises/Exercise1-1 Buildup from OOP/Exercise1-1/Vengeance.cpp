#include "Vengeance.h"
#include "Character.h"

Vengeance::Vengeance(string name)
{
	this->name = name;
}

Vengeance::~Vengeance()
{
}

void Vengeance::cast(Character * caster, Character * target)
{
	this->hpCost = caster->getCurrentHP() / 4;
	cout << "Their Vengeance was unleashed!" << endl;
	cout << caster->getName() << " has sacrificed " << this->hpCost << " HP!" << endl;
	cout << caster->getName() << "s damage has been doubled!" << endl;
	system("pause");
	cout << endl;

	caster->modifyCharacterDamage(caster->getCharacterDamage() * 2);
	caster->modifyCurrentCharacterHP(-(this->hpCost));

	if (caster->getCurrentHP() < 0)
	{
		caster->modifyCurrentCharacterHP(caster->getCurrentHP() - caster->getCurrentHP() + 1); //get to 0 first then add 1 point of HP.
	}
}
